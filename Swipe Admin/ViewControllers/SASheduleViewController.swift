//
//  SASheduleViewController.swift
//  Swipe Admin
//
//  Created by mac on 16/04/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import UIKit
import Alamofire

class SASheduleViewController: BaseViewController {
    @IBOutlet weak var tableViewShedule: UITableView!
    var Arr_StudentData = NSMutableArray()
    var studentDetail = [String: Any]()
    var permissions = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        let login = UserDefaults.standard.getUserID()
              let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
              permissions  = (loginModel?.permissions![0])!
        // Do any additional setup after loading the view.
        self.GetStudentSheduledata_API()
    }
    // MARK: - API methods
       func GetStudentSheduledata_API()  {
            let headers: HTTPHeaders = [
               "SessionId": permissions,
               "Accept": "application/json"
           ]
            Alamofire.request("\(kAPI_BaseURL)\(permissions)/Student/\(studentDetail["StudentId"]!)/Schedule", headers: headers)
               .responseJSON { response in
                 appDelegate.hideHUD(self.view)
                  print("student shcedule datat=\(response)")
                   if response.result.isSuccess
                   {
                       if let result = response.result.value {
                        
                        if let resultarr = result as? NSArray  {
                        if resultarr.count == 0 {
                           // Util.showAlertWithMessage("No Schedule available with search", title: kAPI_Message)
                        } else {
                            self.Arr_StudentData = resultarr.mutableCopy() as! NSMutableArray
                            self.tableViewShedule.reloadData()
                            }

                         } else {
                          //  Util.showAlertWithMessage("No Schedule available with search", title: kAPI_Message)
                           }
                       }
                   }
           }
           appDelegate.hideHUD(self.view)
           self.view.endEditing(true)
           
       }
}

extension SASheduleViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arr_StudentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "StudentCell") as! StudentCell
            let obj = self.Arr_StudentData.object(at: indexPath.row) as! Dictionary<String, Any>
            //SectionCode
            let period = obj["PerName"] as? String;
            let day = obj["DayName"] as? String;
            
            cell1.lblSPeriod.text = "Day " + (day ?? "") + " - " + (period ?? "")
            cell1.lblSRoom.text =  obj["RoomName"] as? String
            //   cell1.lblSRoom.text =  obj["RoomName"] as? String
            cell1.lblSClass.text =  obj["ClassName"] as? String
    
        return cell1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "StudentCellHeader") as! StudentCell
            cell1.lblName.backgroundColor = UIColorFromRGB(rgbValue: 0x0349bd)
            cell1.lblSClass.backgroundColor = UIColorFromRGB(rgbValue: 0x0349bd)
            cell1.lblSPeriod.backgroundColor = UIColorFromRGB(rgbValue: 0x0349bd)
            return cell1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(0.6)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.darkGray
        cell.addSubview(additionalSeparator)
    }
}
