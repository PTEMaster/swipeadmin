//
//  SAConsTableViewController.swift
//  Swipe Admin
//
//  Created by mac on 16/04/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import UIKit
import Alamofire
protocol SAConsTableViewDelegate:NSObject {
    func selectLocationMenu(items:NSDictionary)
}

class SAConsTableViewController: BaseViewController {
    
    @IBOutlet weak var tableViewCon: UITableView!
    
    var Arr_Consequence = NSMutableArray()
    weak var delegate:SAConsTableViewDelegate?
     var permissions = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        permissions  = (loginModel?.permissions![0])!
        // Do any additional setup after loading the view.
        self.GetConsequesce_API()
       // tblConsequece.register(UITableViewCell.self, forCellReuseIdentifier: "cellconsequence")
    }
    
    func GetConsequesce_API()  {
        
        let headers: HTTPHeaders = [
            "SessionId": appDelegate.strSessionID,
            "Accept": "application/json"
        ]
        Alamofire.request("\(kAPI_BaseURL)\(permissions)/CorrectiveActions", headers: headers)
            .responseJSON { response in
                 appDelegate.hideHUD(self.view)
                print("consequence=\(response)")
                if response.result.isSuccess {
                    if let result = response.result.value {
                        
                        let resultarr = result as! NSDictionary
                        if resultarr.count == 0 {
                            
                           // Util.showAlertWithMessage("No Consequence", title: kAPI_Message)
                        } else {
                            let DataArray = resultarr["data"] as! NSArray
                            self.Arr_Consequence = DataArray.mutableCopy() as! NSMutableArray
                            self.tableViewCon.reloadData()
                            print(self.Arr_Consequence)
                        }
                    }
                }
        }
        self.view.endEditing(true)
    }
    
}


extension SAConsTableViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return Arr_Consequence.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "cellconsequence"
        
        // create a new cell if needed or reuse an old one
        let  cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        let obj = self.Arr_Consequence.object(at: indexPath.row) as! Dictionary<String, Any>
        
        cell?.textLabel?.text = obj["Name"] as? String
        cell?.textLabel?.textColor = UIColor.darkGray
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let consequenceDetail = self.Arr_Consequence.object(at: indexPath.row) as! Dictionary<String, Any>
       
        self.delegate?.selectLocationMenu(items: consequenceDetail as NSDictionary)
        self.dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(0.6)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.darkGray
        cell.addSubview(additionalSeparator)
    }
}



