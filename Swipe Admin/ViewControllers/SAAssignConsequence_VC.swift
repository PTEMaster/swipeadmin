//
//  SAAssignConsequence_VC.swift
//  Swipe Admin
//
//  Created by CTInformatics on 12/01/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class SAAssignConsequence_VC: UIViewController {
    
    
    // MARK: - Properties
    var tbllist:UITableView!
    var Arr_Consequence = NSMutableArray()
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblHR:UIButton!
    @IBOutlet weak var lblGR:UIButton!
    @IBOutlet weak var lblID:UILabel!
    @IBOutlet weak var imgPhoto:UIImageView!
    @IBOutlet weak var viewGrade:UIView!
    @IBOutlet weak var viewbasic:UIView!
   
    @IBOutlet weak var btnAddConsequence:UIButton!
     @IBOutlet weak var btnAssignConsequence:UIButton!
    
      var studentDetail = [String: Any]()
     // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.InitialViewsetup()
        Arr_Consequence = ["Class cut detention","Class cut 1111","Class cut 2222","Class cut 3333","Class cut 4444","Class cut 5555"]
        // Do any additional setup after loading the view.
        let strfname = studentDetail["FirstName"] as? String
        let strlname = studentDetail["LastName"] as? String
        
        lblName.text = "\(strfname ?? "") \(strlname ?? "")"
        lblHR.setTitle("HR: \(studentDetail["Homeroom"] as? String ?? "")", for: .normal)
        lblGR.setTitle("GRADE: \(studentDetail["Grade"] as? String ?? "")", for: .normal)
        lblID.text = "\(studentDetail["StudentId"]! as Any)"
        
        tbllist.register(UITableViewCell.self, forCellReuseIdentifier: "cellconsequence")
    }
    
    // MARK: - Action methods
    @IBAction func ClickBack(sender:Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ClickAssignConsequences(sender:Any){
        
    }
    
    @IBAction func Click_SelectConsequences(sender:Any){
        if tbllist.isHidden{
            tbllist.isHidden = false
            tbllist.reloadData()
        }else{
            tbllist.isHidden = true
        }
        
    }
    
    // MARK: - Private methods
    func InitialViewsetup()  {
        viewbasic.dropShadow()
        viewGrade.dropShadow()
        
        imgPhoto.layer.cornerRadius = imgPhoto.frame.height/2
        imgPhoto.layer.borderWidth = 1.5
        imgPhoto.clipsToBounds = true
        imgPhoto.layer.borderColor = UIColor.init(red: 5/255, green: 79/255, blue: 111/255, alpha: 1).cgColor
        viewGrade.layer.borderWidth = 1
        viewGrade.layer.borderColor = UIColor.lightGray.cgColor
        viewGrade.layer.cornerRadius = 14
        
        btnAddConsequence.layer.borderWidth = 1
        btnAddConsequence.layer.borderColor = UIColor.lightGray.cgColor
        btnAddConsequence.layer.cornerRadius = 14
        
        btnAssignConsequence.layer.borderWidth = 1
        btnAssignConsequence.layer.borderColor = UIColor.lightGray.cgColor
        btnAssignConsequence.layer.cornerRadius = 14
        
        
        tbllist = UITableView.init(frame: CGRect(x: btnAddConsequence.frame.origin.x, y: btnAddConsequence.frame.origin.y+140, width: btnAddConsequence.frame.size.width, height: 250))
        tbllist.layer.borderWidth = 1
        tbllist.layer.borderColor = UIColor.darkGray.cgColor
        tbllist.layer.cornerRadius = 14
        tbllist.delegate = self
        tbllist.dataSource = self
        self.view.addSubview(tbllist)
        tbllist.isHidden = true
    }
    
     // MARK: - API methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK: - Tableview delegates
extension SAAssignConsequence_VC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arr_Consequence.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "cellconsequence"
        
        // create a new cell if needed or reuse an old one
        let cell = tbllist.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        cell.textLabel?.text = (Arr_Consequence.object(at: indexPath.row) as! String)
        cell.textLabel?.textColor = UIColor.darkGray
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        btnAddConsequence.setTitle( (Arr_Consequence.object(at: indexPath.row) as! String), for: .normal)
//        let vcObj = storyboard?.instantiateViewController(withIdentifier: "SAStudentDetail_VC") as! SAStudentDetail_VC
//        self.present(vcObj, animated: true, completion: nil)
    }
}
