//
//  SAScansViewController.swift
//  Swipe Admin
//
//  Created by mac on 31/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import UIKit
import Alamofire

class SAScansViewController: BaseViewController {
    @IBOutlet weak var tableDailyLog: UITableView!
    var studentIdentifier = String()
    var studentId = String()
    var arrScans = [ScansModal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getStudentDailyLog_API()
        // Do any additional setup after loading the view.
    }
    func getStudentDailyLog_API()  {
        let headers: HTTPHeaders = ["Accept": "application/json"]
        //GET
                let login = UserDefaults.standard.getUserID()
                let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
               let permissions = (loginModel?.permissions![0])!
       
        Alamofire.request("\(kAPI_BaseURL)\(permissions)/Student/\(studentIdentifier)/DailyLog", headers: headers)
            .responseJSON { response in
                   print("student shcedule datat=\(response)")
                if response.result.isSuccess {
                    if let result = response.result.value {
                        let resultData = result as! [String:Any]
                        let arrResult = resultData["data"] as? [[String:Any]]
                        
                        if 0  < arrResult?.count ?? 0 {
                            for item in arrResult!{
                                let scans = ScansModal(parameters: item)
                                self.arrScans.append(scans)
                                let indexPath = IndexPath(row: self.arrScans.count - 1, section: 0)
                                self.tableDailyLog.insertRows(at: [indexPath], with: .bottom)
                            }
                        } else {
                            self.tableDailyLog.displayBackgroundText(text: "No activity found.")
                        }
                        
                        
                    }
                } 
        }
        appDelegate.hideHUD(self.view)
        self.view.endEditing(true)
        
    }

    @IBAction func actionClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension SAScansViewController: UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrScans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SAScansTableViewCell", for: indexPath) as! SAScansTableViewCell
        let obj = arrScans[indexPath.row]
        cell.lblstudentText.text = obj.text
        cell.lblRecordedby.text = "Recorded by:" + (obj.source ?? "")
        
        return cell
    }
}

class SAScansTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblstudentText: UILabel!
    
    @IBOutlet weak var lblRecordedby: UILabel!
    
}
