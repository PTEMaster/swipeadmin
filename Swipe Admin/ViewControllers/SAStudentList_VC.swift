//
//  SAStudentList_VC.swift
//  Swipe Admin
//
//  Created by CTInformatics on 12/01/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import NetworkExtension
import SDWebImage

extension UIView {
    
    func dropShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 1.5
        layer.shadowOffset = CGSize(width: 0, height: 2.0)
    }
}

class SAStudentList_VC: BaseViewController {
    @IBOutlet weak var tbllist:UITableView!
    @IBOutlet weak var viewNoValue:UIView!
    @IBOutlet weak var txtsearch:UITextField!
    @IBOutlet weak var viewtextsearch:UIView!
    var menuListViewController: SideMenu_VC?

    var strSearch = ""
    var Arr_StudentData = NSMutableArray()

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let sb = UIStoryboard(name: "Main", bundle: nil)
        menuListViewController = (sb.instantiateViewController(withIdentifier: "SideMenu_VC") as! SideMenu_VC)
        menuListViewController!.delegate = self
        txtsearch.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        tbllist.isHidden=true
        viewtextsearch.layer.cornerRadius = viewtextsearch.frame.height/2
        viewtextsearch.layer.borderWidth = 1
        viewtextsearch.clipsToBounds = true
        viewtextsearch.layer.borderColor = UIColor.white.cgColor
        //add obeserver to check device orientation
        tbllist.tableFooterView = UIView()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        txtsearch.delegate = self
        tbllist.register(UINib(nibName: "SAStudentCell", bundle: Bundle.main), forCellReuseIdentifier: "SAStudentCell")
        // Do any additional setup after loading the view.
    }
   
 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        txtsearch.text = ""
        Arr_StudentData.removeAllObjects()
        tbllist.reloadData()
        if Arr_StudentData.count == 0 {
            self.tbllist.isHidden = true
            self.viewNoValue.isHidden = false
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        /*self.navigationController?.view.addSubview(self.menuListViewController!.view)
        self.setupSideMenuAnimations(isAdd: true, toView:  self.menuListViewController!.view)*/
    }
     override func viewDidAppear(_ animated: Bool) {
        
        txtsearch.attributedPlaceholder = NSAttributedString(string: "Student name/ID",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        print(txtsearch.text)
        if CanSearch() {
            SearchAPI()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
       appDelegate.hideHUD(self.view)
    }
    
    @IBAction func actionSideMenu(_ sender: Any) {
        self.navigationController?.view.addSubview( self.menuListViewController!.view)
              self.setupSideMenuAnimations(isAdd: true, toView: self.menuListViewController!.view)
    }
    
    
    
    @IBAction func ClickSearch(sender:Any){
        self.view.endEditing(true)
        if CanSearch() {
            SearchAPI()
        }
    }
    
    func setupSideMenuAnimations(isAdd:Bool, toView:UIView) {
         if isAdd {
            toView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: self.view.frame.size.height)
             
            toView.frame.origin.y =  (self.view.frame.origin.y)
             toView.frame.origin.x = -self.view.frame.width
             toView.alpha = 0
             UIView.animate(withDuration: 1.0) {
                 toView.frame.origin.x = 0
                 toView.alpha = 1
                self.view.addSubview(toView)
             }
         }
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - API methods
    func SearchAPI()  {
      //  SDImageCache.shared.clearMemory()
      //  SDImageCache.shared.clearDisk()
        appDelegate.showHUD("", onView: self.view)
        let headers: HTTPHeaders = [
            "SessionId": appDelegate.strSessionID,
            "Accept": "application/json"
        ]
     //   print("headers-\(headers)-")
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        Alamofire.request("\(kAPI_BaseURL)\((loginModel?.permissions![0])!)/Students/Find?criteria=\(txtsearch.text!)", headers: headers)
            .responseJSON { response in

                if response.result.isSuccess
                {
                  //  print("Response-\(response.result.value!)-")
                    if let result = response.result.value {
                        self.Arr_StudentData.removeAllObjects()
                        let resultarr = result as! NSArray
                        if resultarr.count == 0
                        {
                            self.tbllist.isHidden = true
                            self.viewNoValue.isHidden = false
                          Util.showAlertWithMessage("No Student available with name/Id you entered", title: kAPI_Message)
                        }else
                        {
                            self.tbllist.isHidden=false
                            self.viewNoValue.isHidden=true
                           let ArrStudentData = resultarr.mutableCopy() as! NSMutableArray
                            for i in 0..<ArrStudentData.count {
                                let dict = ArrStudentData[i] as! NSDictionary
                         //    print(dict)
                                //added check  to remove teacher in listing hp
                               /* if dict["Grade"]as! String != "STAFF"{
                                    self.Arr_StudentData.add(dict)
                                }*/
                                self.Arr_StudentData.add(dict)
                            }
                            print(self.Arr_StudentData)
                            
                            if self.Arr_StudentData.count == 0
                            {
                                self.tbllist.isHidden = true
                                self.viewNoValue.isHidden = false
                            //  Util.showAlertWithMessage("No Student available with name/Id you entered", title: kAPI_Message)
                            }
                            
                            self.tbllist.reloadData()
                        }
                    }
        }
        
    appDelegate.hideHUD(self.view)
    }
        //self.view.endEditing(true)
        
    }
    // MARK: - Private methods
    func CanSearch() -> Bool {
        if (txtsearch.text?.isEmpty)! {
           // Util.showAlertWithMessage("Please enter username", title: kAPI_Message)
         Util.showAlertWithMessage("Please insert Name/Id to search", title: kAPI_Message)
            return false
        }
        return true
    }

}
//MARK: - Side menu delegates

extension SAStudentList_VC: SidemenuDelegate {
    func didSelectRowAt(indexPath:IndexPath) {
        let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey")
        if Int(LoginSurvey!) ?? 0 > 0{
            //himanshu
            if indexPath.row == 0 {
                if #available(iOS 13.0, *) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "SALocationScanViewController") as! SALocationScanViewController
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .coverVertical
                    self.present(vc, animated: true, completion: nil)
                } else {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SALocationScanViewController") as! SALocationScanViewController
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .coverVertical
                    self.present(vc, animated: true, completion: nil)
                }
            }
            if indexPath.row == 1 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SAStudentStaffList_VC") as! SAStudentStaffList_VC
                   self.navigationController?.pushViewController(vc, animated: true)
            }
           else  if indexPath.row == 2 {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SAScanningList_VC") as! SAScanningList_VC
                self.navigationController?.pushViewController(vc, animated: true)
            /*   let vc = self.storyboard?.instantiateViewController(withIdentifier: "SAManualViewControllerListing") as! SAManualViewControllerListing
                   self.navigationController?.pushViewController(vc, animated: true)
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(identifier: "SAManualViewControllerListing") as! SAManualViewControllerListing
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SAManualViewControllerListing") as! SAManualViewControllerListing
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: nil)
            }*/
            }
            else {
          //    print("Logout")
                self.actionLogout()
            }
       }else{
        if indexPath.row == 0 {
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(identifier: "SALocationScanViewController") as! SALocationScanViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SALocationScanViewController") as! SALocationScanViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: nil)
            }
        } else {
        //  print("Logout")
            self.actionLogout()
        }
        }
    }
}
//MARK: - Tableview delegates
extension SAStudentList_VC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arr_StudentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SAStudentCell") as! SAStudentCell
        let obj = self.Arr_StudentData.object(at: indexPath.row) as! [String : Any]
       
        cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.height/2
        cell.imgPhoto.layer.borderWidth = 2
        cell.imgPhoto.clipsToBounds = true
        cell.imgPhoto.layer.borderColor = UIColorFromRGB(rgbValue: 0x28a8f9).cgColor
        let randomString = "?" +  NSUUID().uuidString
        let strimageurl = "https://" + (obj["imageHost"] as? String ?? "")  + (obj["imageUrl"] as? String ?? "") +  randomString
      print("strimageurl-\(strimageurl)-")
       // cell.imgPhoto.sd_imageIndicator?.startAnimatingIndicator()
        cell.imgPhoto.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPhoto.sd_setImage(with: URL(string: strimageurl), placeholderImage: UIImage(named: "profile"), options: .queryDiskDataSync, progress: .none, completed: nil)
        
       
        let strfname = obj["FirstName"] as? String
        let strlname = obj["LastName"] as? String
        cell.lblName.text = "\(strfname ?? "") \(strlname ?? "")"
        cell.lblName.textColor = UIColorFromRGB(rgbValue: 0x28a8f9)
        if let grade = obj["Grade"] as? String {
            cell.lblGR.text = grade
        } else  {
            cell.lblGR.text = "0"
        }
        
        if let homeroom = obj["Homeroom"]
        {
            cell.lblHR.text = homeroom as? String;
        }else{
            cell.lblHR.text = "";
        }
        cell.lblID.text = "\(obj["StudentNumber"]! as Any)"
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 1.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.dropShadow()
        cell.btnnext.layer.cornerRadius = cell.btnnext.frame.height/2
        cell.btnnext.clipsToBounds = true
        cell.btnnext.tag = indexPath.row
        cell.btnnext.addTarget(self, action: #selector(actionStudentDetail), for: .touchUpInside)
        return cell
    }
    
    @objc func actionStudentDetail(sender:UIButton) {
        let indexPath = sender.tag
        var obj = self.Arr_StudentData.object(at: indexPath) as! [String : Any]
       // paramsDict = obj
        obj.removeValue(forKey: "__type")
       // paramsDict = obj
        if obj["Grade"]as! String == "STAFF"{
            print("staff")
           // self.performSegue(withIdentifier: "identifierstudentDetails", sender: obj)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SAStaffDetail_VC") as! SAStaffDetail_VC
            vc.studentDetail = obj
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.performSegue(withIdentifier: "identifierstudentDetails", sender: obj)
        }
         
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var obj = self.Arr_StudentData.object(at: indexPath.row) as! [String : Any]
        obj.removeValue(forKey: "__type")
        
        if obj["Grade"]as! String == "STAFF"{
            print("staff")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SAStaffDetail_VC") as! SAStaffDetail_VC
            vc.studentDetail = obj
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SAStudentDetail_VC") as! SAStudentDetail_VC
            vc.studentDetail = obj
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "identifierstudentDetails" {
            let vc = segue.destination as? SAStudentDetail_VC
            vc?.studentDetail = sender as! [String:Any]
        }
    }
    
}

extension SAStudentList_VC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if CanSearch() {
            SearchAPI()
        }
    }
}
/*{
 let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey")
 if LoginSurvey == "1"{
     //home
     
     if indexPath.row == 0 {
        // self.navigationController?.popToRootViewController(animated: true)
         for controller in self.navigationController!.viewControllers as Array {
             if controller.isKind(of: SAStudentList_VC.self) {
                 self.navigationController!.popToViewController(controller, animated: true)
                 break
             }
         }
     }
     // student list
    else if indexPath.row == 1 {
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "SALocationScanViewController") as! SALocationScanViewController
         vc.modalPresentationStyle = .overFullScreen
         vc.modalTransitionStyle = .coverVertical
         self.present(vc, animated: true, completion: nil)
     }
     else  if indexPath.row == 2 {
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "SAStudentStaffList_VC") as! SAStudentStaffList_VC
            self.navigationController?.pushViewController(vc, animated: true)
     }else {
       print("Logout")
         self.actionLogout()
     }
}else{
 if indexPath.row == 0 {
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "SALocationScanViewController") as! SALocationScanViewController
     vc.modalPresentationStyle = .overFullScreen
     vc.modalTransitionStyle = .coverVertical
     self.present(vc, animated: true, completion: nil)
     
 }   else if indexPath.row == 1 {
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "SALocationScanViewController") as! SALocationScanViewController
     vc.modalPresentationStyle = .overFullScreen
     vc.modalTransitionStyle = .coverVertical
     self.present(vc, animated: true, completion: nil)
     
 } else {
   print("Logout")
     self.actionLogout()
 }
 }
}*/


