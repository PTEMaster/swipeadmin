//
//  ManualViewController.swift
//  Swipe Admin
//
//  Created by mac on 18/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import SDWebImage
class SAManualViewController: BaseViewController , UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableManual: UITableView!
    @IBOutlet weak var lblStudentName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet var ViewPop: UIView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet var viewNotData: UIView!
    var locationData: GetLocationModal?
    var arrStudent = [StudentModal]()
    var scanlocation = SALocationScanViewController()
    var page:Int?
    var isFilter:String?
    static let notificationName = Notification.Name("getLocationdata")
   var pointContentOffset = CGPoint.zero
    
     var player: AVAudioPlayer??
    //1
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        // Do any additional setup after loading the view.
        tableManual.register(UINib(nibName: "SAStudentCell", bundle: Bundle.main), forCellReuseIdentifier: "SAStudentCell")
        page = 1
        isFilter = "lastName"
        self.studentListAPI(txtsearch: searchBar.text!, page: page!)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: SAManualViewController.notificationName, object: nil)
        btnFilter.setTitle("Last Name", for: .normal)
       
        
       
    }
    //MARK:- UIButton actions
    @IBAction func actionFilter(_ sender: UIButton) {
        let alertController = storyboard?.instantiateViewController(withIdentifier: "SAMenuViewController") as! SAMenuViewController
        alertController.modalPresentationStyle = .popover
        alertController.arrFilterList = [FilterModal(dispaly: "Last Name", filter: "lastName"), FilterModal(dispaly: "Student Number", filter: "studentNumber")]
        alertController.isFilter = true
        alertController.delegate = self
        let height = self.view.frame.size.height / 3
        let width = self.view.frame.size.width / 2
        alertController.preferredContentSize = CGSize(width: width, height: height)
        let popover = alertController.popoverPresentationController
        popover?.permittedArrowDirections =  .up
        popover?.delegate = self
        popover?.sourceView = sender
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Custom funcation
    func setupSoundValidandInvalid(sound:String) {
        let url = Bundle.main.url(forResource: sound, withExtension: "mp3")!
        do {
            //Preparation to play
            do {
                
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
                do {
                    try AVAudioSession.sharedInstance().setActive(true)
                    //print("AVAudioSession is Active")
                } catch _ as NSError {
                    //print(error.localizedDescription)
                }
            } catch let error {
                print("Error in AVAudio Session\(error.localizedDescription)")
            }
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player?.prepareToPlay()
            player?.play()
            
        } catch let error as NSError {
            print(error.description)
        }
    }
    func setupPopView(status:Bool, toView:UIView) {
           toView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60)
             toView.frame.origin.y = self.view.frame.height
             self.view.addSubview(toView)
             UIView.animate(withDuration: 0.5) {
                 toView.frame.origin.y = self.view.frame.height - (toView.frame.height)
              // print(toView.frame)
             }
             
             DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                 UIView.animate(withDuration: 0.5, delay: 0.2, options: [.curveEaseInOut], animations: {
                     // self.statusCheck = true
                 }) { (bool) in
                     toView.removeFromSuperview()
                 }
             }
         }

    @objc func onNotification(notification:Notification) {
         SelectLocationManager.shard.selectLocation = (notification.userInfo!["data"] as! GetLocationModal)
        
         locationData = (notification.userInfo!["data"] as! GetLocationModal)
     }
     
     
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         
     }
     // MARK:- SearchBar Delegate methods
     func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
         searchBar.text = ""
         searchBar.resignFirstResponder()
        
         if 0 < arrStudent.count {
            self.tableManual.reloadData()
         } else {
             self.arrStudent.removeAll()
             self.tableManual.reloadData()
             page = 1
             isFilter = "lastName"
             self.studentListAPI(txtsearch: searchBar.text!, page: page!)
         }
     }
     
     func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
         if !searchBar.text!.isEmpty {
             page = 1
             self.arrStudent.removeAll()
             self.tableManual.reloadData()
             self.studentListAPI(txtsearch: searchBar.text!, page: page!)
         }
     }
    
    // MARK: - API methods
    func studentListAPI(txtsearch:String , page:Int)  {
        
       appDelegate.showHUD("", onView: self.view)
        let headers: HTTPHeaders = [
            "SessionId": appDelegate.strSessionID,
            "Accept": "application/json"
        ]
        
      //  arrStudent.removeAll()
       // tableManual.reloadData()
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        let getURlPage = "?format:json" + "&filterValue=\(txtsearch)" + "&filterField=\(isFilter ?? "None")" + "&take=11" + "&skip=0" + "&page=\(page)" + "&pageSize=11"
        let baseUrl = "\(kAPI_BaseURL)\((loginModel?.permissions![0])!)/Students" +  getURlPage
       print(baseUrl)
        Alamofire.request(baseUrl , headers: headers)
            .responseJSON { response in
                appDelegate.hideHUD(self.view)
               // print(response)
                if response.result.isSuccess
                {
                    if let result = response.result.value {
                         let studentData = result as? [String:Any]
                        if let studentlist = studentData!["data"] as? [[String:Any]]  {
                         print("studentlist-\(studentlist)-")
                             self.tableManual.backgroundView?.removeFromSuperview()
                            if 0 < studentlist.count {
                                self.tableManual.isHidden = false
                                self.viewNotData.isHidden = true
                                for student in studentlist {
                                let studentModal = StudentModal(parameters: student)
                                self.arrStudent.append(studentModal)
//                                    print(self.arrStudent[0].Homeroom)
                               // let indexPath = IndexPath(row: self.arrStudent.count - 1, section: 0)
                               // self.tableManual.insertRows(at: [indexPath], with: .bottom)
                                }
                                self.tableManual.reloadData()
                            } else {
                                self.tableManual.isHidden = true
                                self.viewNotData.isHidden = false
                            }
                        } else {
                            self.tableManual.isHidden = true
                            self.viewNotData.isHidden = false
                        }
                    }
                     
                }
                
               // appDelegate.hideHUD(self.view)
        }
        self.view.endEditing(true)
    }

}


//MARK: - Tableview delegates
extension SAManualViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStudent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SAStudentCell") as! SAStudentCell
        //UI Setting
        cell.selectionStyle = .none

        let obj = arrStudent[indexPath.row]
        cell.btnnext.layer.cornerRadius = cell.btnnext.frame.height / 2
        cell.btnnext.clipsToBounds = true
        cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.height / 2
        cell.imgPhoto.layer.borderWidth = 2
        cell.imgPhoto.clipsToBounds = true
        cell.imgPhoto.layer.borderColor = UIColorFromRGB(rgbValue: 0x28a8f9).cgColor
    
        let strimageurl = "https://app.swipek12.com" + (obj.imageUrl ?? "")

        cell.imgPhoto.image = nil
        cell.imgPhoto.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPhoto.sd_setImage(with: URL(string: strimageurl), placeholderImage: UIImage(named: "profile"), options: .queryDiskDataSync, progress: .none, completed: nil)

        let strfname = obj.FirstName
        let strlname = obj.LastName
        cell.lblName.text = "\(strfname ?? "Student") \(strlname ?? "Student1")"
        cell.lblName.textColor = UIColorFromRGB(rgbValue: 0x28a8f9)
       
        cell.lblGR.text = "\(obj.Grade ?? "0")"
        
        if let homeroom = obj.Homeroom {
            cell.lblHR.text = "\(homeroom)"
        } else {
            cell.lblHR.text = "";
        }
        
        cell.lblID.text = "\(obj.StudentNumber ?? "0" )"
        cell.viewbackground.layer.cornerRadius = 6
        cell.viewbackground.layer.borderColor = UIColor.lightGray.cgColor
        cell.viewbackground.layer.borderWidth = 0.5
        cell.viewbackground.dropShadow()
        
        cell.btnnext.tag = indexPath.row
        cell.btnnext.addTarget(self, action: #selector(actionPrsentStudent), for: .touchUpInside)
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrStudent.count - 1) {
            page! += 1
            self.studentListAPI(txtsearch: "", page: page!)
        }
    }
    
    
    @objc func actionPrsentStudent(sender:UIButton) {
        let object = arrStudent[sender.tag]
        self.lblStudentName.text = (object.FirstName ?? "" ) + (object.LastName ?? "")
        
        self.isSubmitWebScanApiCall(studentId: object.StudentNumber!)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = arrStudent[indexPath.row]
        
       
       
        self.lblStudentName.text = (object.FirstName ?? "" ) + (object.LastName ?? "")
        let scanLocation = object.Homeroom
        
        self.isSubmitWebScanApiCall(studentId: object.StudentNumber! )
    }
    
    func isSubmitWebScanApiCall(studentId:String ) {
        let url = "\(kAPI_BaseURL)Attendance/SubmitWebScan"
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        let permissions = (loginModel?.permissions![0])!
        var params = [String:Any]()
        params["delimitedStudentString"] =  studentId
        params["schoolId"] = permissions
        params["scanLocation"] = locationData?.RoomName
        params["classLocationScanned"] = false
        params["commandedBy"] = UserDefaults.standard.getUserName()
        
        if ((locationData?.RoomName) == nil){
            params["scanLocation"] = SelectLocationManager.shard.selectLocation.RoomName
        }else{
            params["scanLocation"] = locationData?.RoomName
        }
       // print(params)
        let headers: HTTPHeaders = ["SessionId": appDelegate.strSessionID,
                                    "Accept": "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        //    print(response)
            if response.result.isSuccess {
                if let result = response.result.value {
               //     print(result)
                    let date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "HH:mm"
                    let currentTime = dateFormatter.string(from: date)
                    self.lblTime.text = "\(currentTime)"
                    self.ViewPop.backgroundColor = .green
                    self.setupSoundValidandInvalid(sound: "validId")
                    self.setupPopView(status: true, toView: self.ViewPop)
                }
            }
        }
    }
    
}
extension SAManualViewController:UIPopoverPresentationControllerDelegate , MenuDelegate {
    
    func selectFilterMenu(filter: FilterModal) {
        self.btnFilter.setTitle(filter.dispalyName, for: .normal)
        self.isFilter = filter.filterName
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
         return .none
     }

        
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
         return true
    }
}

