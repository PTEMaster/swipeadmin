//
//  SAStudentDetail_VC.swift
//  Swipe Admin
//
//  Created by CTInformatics on 12/01/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import MobileCoreServices
import SDWebImage
extension Date {
    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
        return targetDay
    }
    
    /// Returns a Date with the specified days subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        let inverseYears = -1 * years
        let inverseMonths = -1 * months
        let inverseDays = -1 * days
        let inverseHours = -1 * hours
        let inverseMinutes = -1 * minutes
        let inverseSeconds = -1 * seconds
        return add(years: inverseYears, months: inverseMonths, days: inverseDays, hours: inverseHours, minutes: inverseMinutes, seconds: inverseSeconds)
    }
    
}

var paramsDict = [String:Any]()


class SAStudentDetail_VC: BaseViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    // MARK: - Property
   // @IBOutlet weak var tbllist:UITableView!
    //var tblConsequece:UITableView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblHR:UILabel!
    @IBOutlet weak var lblGR:UILabel!
    @IBOutlet weak var lblID:UILabel!
    @IBOutlet weak var lblActive:UILabel!
    @IBOutlet weak var imgPhoto:UIImageView!
    @IBOutlet weak var viewGrade:UIView!
    @IBOutlet weak var viewbasic:UIView!
    @IBOutlet weak var viewstack:UIStackView!
    @IBOutlet weak var btnConsequence:UIButton!
    @IBOutlet weak var btnViewdetail:UIButton!
    @IBOutlet weak var btnSelectConswquence: UIButton!
    @IBOutlet weak var btnAssignConsequence: UIButton!
    @IBOutlet weak var tableViewShedule: UITableView!
    
    var ISDetailShown:Bool = true
    var imageData:Data!
    var consequenceDetail = [String:Any]()
    var arr_Consequence = NSMutableArray()
  //  @IBOutlet weak var viewDetail:UIView!
    var studentDetail = [String: Any]()
    var ConsequenceDetail = [String: Any]()
    var strStudentID:String!
    var Arr_StudentData = NSMutableArray()
    var Arr_Consequence = NSMutableArray()
    let picker = UIImagePickerController()
    var permissions = String()
    @IBOutlet weak var viewShedule: UIView!
    @IBOutlet weak var viewConsequence: UIView!
    
     static let notification = Notification.Name("PushDataViewController")
  
    // MARK: - Life cycle
    fileprivate func isSetData() {
        appDelegate.hideHUD(self.view)
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        permissions  = (loginModel?.permissions![0])!
        
        appDelegate.showHUD("", onView: self.view)
        picker.delegate = (self as UIImagePickerControllerDelegate & UINavigationControllerDelegate)
        let strfname = studentDetail["FirstName"] as? String
        let strlname = studentDetail["LastName"] as? String
        lblName.text = "\(strfname ?? "") \(strlname ?? "")"
        
        lblName.textColor = UIColorFromRGB(rgbValue: 0x28a8f9)
        if let homeroom = studentDetail["Homeroom"] as? String {
            lblHR.text = "HR: " +  homeroom
        } else {
            lblHR.text = "HR: N/A"
        }
        
        lblGR.text = "GRADE: \(studentDetail["Grade"] as? String ?? "")"
        lblID.text = " \(studentDetail["StudentNumber"] as? String ?? "")"
        let randomString = "?" +  NSUUID().uuidString
        let strimageurl = "https://" + (studentDetail["imageHost"] as? String ?? "")  + (studentDetail["imageUrl"] as? String ?? "") +  randomString
        imgPhoto.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgPhoto.sd_setImage(with: URL(string: strimageurl), placeholderImage: UIImage(named: "profile"), options: .queryDiskDataSync, progress: .none, completed: nil)
        
      
        self.viewConsequence.isHidden = true
        self.InitialviewSetup()
        GetStudentStatus_API()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isSetData()
        // Do any additional setup after loading the view.
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
   
    // MARK: - Private methods
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style:.default, handler: nil)
        alertVC.addAction(okAction)
        present( alertVC, animated: true, completion: nil)
    }
    func photoFromLibrary() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = [kUTTypeImage as String] //UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    func shootPhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            picker.cameraCaptureMode = .photo
            picker.allowsEditing = true
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            noCamera()
        }
    }
    func InitialviewSetup()
    {
        imgPhoto.layer.cornerRadius = imgPhoto.frame.height/2
        imgPhoto.layer.borderWidth = 2
        imgPhoto.clipsToBounds = true
        imgPhoto.layer.borderColor = UIColorFromRGB(rgbValue: 0x28a8f9).cgColor

        viewGrade.layer.borderWidth = 1
        viewGrade.layer.borderColor = UIColor.lightGray.cgColor
        viewGrade.layer.cornerRadius = viewGrade.frame.height/2
        
        viewbasic.layer.borderWidth = 2
        viewbasic.layer.borderColor = UIColor.lightGray.cgColor

        viewstack.layer.borderColor = UIColor.darkGray.cgColor
        viewstack.layer.borderWidth = 1
        btnConsequence.backgroundColor = UIColor.darkGray
        btnViewdetail.backgroundColor = UIColorFromRGB(rgbValue: 0x28a8f9)
    }
    @IBAction func actionsScans(_ sender: Any) {
        self.performSegue(withIdentifier: "gotoScansViewController", sender: sender)
    }
    
    @IBAction func actionConsequence(_ sender: UIButton) {
         let alertController = storyboard?.instantiateViewController(withIdentifier: "SAConsTableViewController") as! SAConsTableViewController
         alertController.modalPresentationStyle = .popover
         alertController.delegate = self
         alertController.Arr_Consequence = self.arr_Consequence
         let height = self.view.frame.size.height / 2
         let width = sender.frame.size.width
         alertController.preferredContentSize = CGSize(width: width, height: height)
         let popover = alertController.popoverPresentationController
         popover?.permittedArrowDirections =  .up
         popover?.delegate = self
         popover?.sourceView = sender
         self.present(alertController, animated: true, completion: nil)
     }
     @IBAction func actionAssignConsequence(_ sender: UIButton) {
         self.assignConsequenceAPI()
     }
     
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoScansViewController" {
            let vc = segue.destination as? SAScansViewController
            vc?.studentIdentifier =  studentDetail["UniqueIdentifier"] as? String ?? ""
            vc?.studentId =  studentDetail["StudentId"] as? String ?? ""
        }
    }
    // MARK: - API methods
     @objc func GetStudentStatus_API() {
        let headers: HTTPHeaders = [
            "SessionId": appDelegate.strSessionID,
            "Accept": "application/json"
        ]
        print("\(kAPI_BaseURL)\(permissions)/Student?studentNumber=\(studentDetail["StudentNumber"] as AnyObject)")
        Alamofire.request("\(kAPI_BaseURL)\(permissions)/Student?studentNumber=\(studentDetail["StudentNumber"] as AnyObject)", headers: headers)
            .responseJSON { response in
                appDelegate.hideHUD(self.view)
                if response.result.isSuccess
                {
                    if let result = response.result.value {
                        print(result)
                        let resultarr = result as! NSDictionary
                        if let dataarray = resultarr["data"] as? NSDictionary {
                            if dataarray["Active"]! as! String == "Y" {
                                //  self.lblActive.text = "Present"
                                
                                if let entryarray = dataarray["CurrentDayStatus"]
                                {
                                    print(entryarray)
                                    let entryarray1 = dataarray["CurrentDayStatus"] as! NSDictionary
                                    //
                                    var strentry = entryarray1["EntryDate"]  as! String
                                    let StatusName =   entryarray1["StatusName"]  as? String ?? "N/A"
                                    if let range = strentry.range(of: "/Date(") {
                                        strentry.removeSubrange(range)
                                    }
                                    
                                    if let range = strentry.range(of: "-0000)/") {
                                        strentry.removeSubrange(range)
                                    }
                                    let millissecond = Double(strentry)
                                    let date = Date(timeIntervalSince1970: (millissecond!/1000.0))
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
                                //    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                                    let strdate = dateFormatter.string(from: date)
                                    
                                    let datenew = dateFormatter.date(from:strdate )
                                   // dateFormatter.dateFormat = "hh:mm a"
                                    dateFormatter.dateFormat = "yyyy/MM/dd"
                                    //self.lblActive.text = StatusName + " " + "\(dateFormatter.string(from: datenew!))"
                                    self.lblActive.text = StatusName
                                    self.lblActive.textColor = UIColor.blue
                                    self.GetStudentSheduledata_API()
                                } else {
                                    self.GetStudentSheduledata_API()
                                    self.lblActive.text = "No Record"
                                    //self.lblActive.textColor = UIColor.red
                                }
                            }
                       
                        } else{
                            self.lblActive.text = "No Record"
                           // self.lblActive.textColor = UIColor.systemBackground
                        }
                         debugPrint(resultarr)
                    }
                }
        }
        self.view.endEditing(true)
    }
  
     func UploadPhoto() {
        appDelegate.showHUD("", onView: self.view)
        let postParams: [String: AnyObject] =
            [
                "SchoolId" : permissions as AnyObject,
                "StudentNumber" : studentDetail["StudentNumber"] as AnyObject,
        ]
        
        //let header = ["Content-Type":"application/x-www-form-urlencoded" ]
        
      let headers = ["content-type": "application/json"]
        /*,
        "Content-Type": "application/json",
        "Accept": "application/json"*/
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(self.imageData, withName: "profile_pic", fileName: UUID().uuidString + ".jpg", mimeType: "image/jpg")
            for (key, value) in postParams {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, usingThreshold: UInt64.init() ,to:"https://evergreen.swipek12.com/photoservice/upload", method: .post, headers: headers)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                      appDelegate.hideHUD(self.view)
                    //print(response.value as Any)
                    print("Response-\(response.result.value!)-")
                    let strError = String(data: response.data!, encoding: .utf8)
                    
                 //   print(strError as Any)
                    
                 //   print(response.response as Any) // URL response
                    self.performAlertViewController(title: AppName, message: "Photo Uploaded Successfully!")
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
           
    }
    // MARK: - API GetStudentSheduledata_API
    func GetStudentSheduledata_API()  {
       // appDelegate.showHUD("Looding..", onView: self.view)
        let headers: HTTPHeaders = [
            "SessionId": permissions,
            "Accept": "application/json"
        ]
        Alamofire.request("\(kAPI_BaseURL)\(permissions)/Student/\(studentDetail["StudentId"]!)/Schedule", headers: headers)
            .responseJSON { response in
                appDelegate.hideHUD(self.view)
                print("student shcedule datat=\(response)")
                
                if response.result.isSuccess
                {
                    if let result = response.result.value {
                        self.GetConsequesce_API()
                        if let resultarr = result as? NSArray  {
                            if resultarr.count == 0 {
                                 Util.showAlertWithMessage("No Schedule available with search", title: kAPI_Message)
                            } else {
                                self.Arr_StudentData = resultarr.mutableCopy() as! NSMutableArray
                                self.tableViewShedule.reloadData()
                                
                            }
                            
                        } else {
                            //  Util.showAlertWithMessage("No Schedule available with search", title: kAPI_Message)
                        }
                    }
                }
        }
        appDelegate.hideHUD(self.view)
        self.view.endEditing(true)
        
    }

      func GetConsequesce_API()  {
          let headers: HTTPHeaders = [
              "SessionId": appDelegate.strSessionID,
              "Accept": "application/json"
          ]
          appDelegate.showHUD("", onView: self.view)
          Alamofire.request("\(kAPI_BaseURL)\(permissions)/CorrectiveActions", headers: headers)
              .responseJSON { response in
                  appDelegate.hideHUD(self.view)
                print("consequence=\(response)")
                  if response.result.isSuccess {
                      if let result = response.result.value {
                          
                          let resultarr = result as! NSDictionary
                          if resultarr.count == 0 {
                            
                             // Util.showAlertWithMessage("No Consequence", title: kAPI_Message)
                          } else {
                              if resultarr["status"] as? Bool == true {
                                  if  let DataArray = resultarr["data"] as? NSArray {
                                      self.arr_Consequence = DataArray.mutableCopy() as! NSMutableArray
                                      let consequenceS = self.arr_Consequence.object(at: 0) as! [String:Any]
                                      self.consequenceDetail = consequenceS
                                      let stringAssign = consequenceS["Name"] as? String
                                      self.btnSelectConswquence.setTitle(stringAssign, for: .normal)
                                  }
                              } else {
                              // Util.showAlertWithMessage("No Consequence", title: kAPI_Message)
                              }
                             

                          }
                      }
                  }
          }
          self.view.endEditing(true)
      }
    
    func assignConsequenceAPI() {
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        let today = Date() // date is then today for this example
        //let days = consequenceDetail["ServeByDays"]  as? Int
        // let tomorrow = today.add(days: days!)
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date  1/17/2018
        formatter.dateFormat = "MM/dd/yyyy"
        //let myString = formatter.string(from: tomorrow)
        let currentdate = formatter.string(from: today)
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        permissions  = (loginModel?.permissions![0])!
        let postParams: [String: AnyObject] =
            [
                "SchoolId" : permissions as AnyObject,
                "StudentNumber" : studentDetail["StudentNumber"] as AnyObject,
                "AggregateId" : studentDetail["UniqueIdentifier"] as AnyObject,
                "SubmitBy" : loginModel?.userName as AnyObject,
                "Type" : "0" as AnyObject,
                "ServeBy" : currentdate as AnyObject,
                "Text" : consequenceDetail["Name"] as AnyObject,
                "Units" : "1" as AnyObject,
                "DateAdded" : currentdate as AnyObject
        ]
        Networking.performApiCall(Networking.Router.AssignConsequence((postParams as [String : AnyObject]?)!), callerObj: self, showHud:true) { (response) -> () in
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess {
                if let result = response.result.value {
                    let resultarr = result as! NSDictionary
                    //print("assign consequence=\(result)")
                    Util.showAlertWithMessage(resultarr["responseText"] as! String, title: kAPI_Message)
                }else
                {
                    Util.showAlertWithMessage("Not Assign", title: kAPI_Message)
                }
            }
        }
    }

    // MARK: - action methods
    @IBAction func ClickConsequence(sender:Any){
        ISDetailShown=false
        btnConsequence.backgroundColor = UIColorFromRGB(rgbValue: 0x28a8f9)
        self.viewConsequence.isHidden = false
        self.viewShedule.isHidden = true
        btnViewdetail.backgroundColor = UIColor.darkGray
    }
 
    @IBAction func ClickViewDetail(sender:Any){
        self.GetStudentSheduledata_API()
        self.viewConsequence.isHidden = true
        self.viewShedule.isHidden = false
        btnConsequence.backgroundColor = UIColor.darkGray
        btnViewdetail.backgroundColor = UIColorFromRGB(rgbValue: 0x28a8f9)
    }
    
    @IBAction func SelectPhoto(sender:UIButton){
      //  tblConsequece.isHidden=true
        if sender.tag == 0 {
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.alert)
            
            actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                self.shootPhoto()
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                self.photoFromLibrary()
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            
            self.present(actionSheet, animated: true, completion: nil)
        }else{
            let vcObj = storyboard?.instantiateViewController(withIdentifier: "SAAssignConsequence_VC") as! SAAssignConsequence_VC
            self.present(vcObj, animated: true, completion: nil)
        }
    }
    
    //MARK: - Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as? UIImage {
                  // Use editedImage Here
                  imgPhoto.image = editedImage
                  self.imageData = imgPhoto.image!.jpegData(compressionQuality: 1.0)
            
            
            
            self.imageData =  resize(editedImage)
           // let  imageSize = (self.imageData.count) / 1024
                           // print("image size:- ",imageSize)
                  //mediatype = MediaType(rawValue: MediaType.Image.rawValue)
                  DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                      // your code here
                      self.UploadPhoto()
                     // self.uploadImageRequest(image:editedImage)
                  }
                  
        } else if (info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage) != nil {
                  // Use originalImage Here
              }
              dismiss(animated:true, completion: nil)
    }
  
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    


}

//Shedule Student
//MARK:- UITableViewDelegate and DataSource methods
extension SAStudentDetail_VC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arr_StudentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "StudentCell") as! StudentCell
            let obj = self.Arr_StudentData.object(at: indexPath.row) as! Dictionary<String, Any>
            //SectionCode
            let period = obj["PerName"] as? String;
            let day = obj["DayName"] as? String;
            
            cell1.lblSPeriod.text = "Day " + (day ?? "") + " - " + (period ?? "")
            cell1.lblSRoom.text =  obj["RoomName"] as? String
            //   cell1.lblSRoom.text =  obj["RoomName"] as? String
            cell1.lblSClass.text =  obj["ClassName"] as? String
    
        return cell1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "StudentCellHeader") as! StudentCell
            cell1.lblName.backgroundColor = UIColorFromRGB(rgbValue: 0x0349bd)
            cell1.lblSClass.backgroundColor = UIColorFromRGB(rgbValue: 0x0349bd)
            cell1.lblSPeriod.backgroundColor = UIColorFromRGB(rgbValue: 0x0349bd)
            return cell1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(0.6)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.darkGray
        cell.addSubview(additionalSeparator)
    }
}

//MARK:- UIPopover PresentationController Delegate
extension SAStudentDetail_VC:UIPopoverPresentationControllerDelegate , SAConsTableViewDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
         return .none
     }

        
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
         return true
    }
    
    func selectLocationMenu(items: NSDictionary) {
        let stringAssign = items["Name"] as? String
        btnAssignConsequence.isHidden = false
        btnSelectConswquence.setTitle(stringAssign, for: .normal)
        consequenceDetail = items as! [String : Any]
    }
 
}

extension Dictionary {
    var jsonStringRepresentation: String? {
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,
                                                            options: [.prettyPrinted]) else {
            return nil
        }

        return String(data: theJSONData, encoding: .ascii)
    }
}
extension Double {
    func getDateStringFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium

        return dateFormatter.string(from: date)
    }
}


func resize(_ image: UIImage) -> Data {
    var actualHeight = Float(image.size.height)
    var actualWidth = Float(image.size.width)
    let maxHeight: Float = 800.0
    let maxWidth: Float = 800.0
    var imgRatio: Float = actualWidth / actualHeight
    let maxRatio: Float = maxWidth / maxHeight
    let compressionQuality: Float = 0.9
    //50 percent compression
    if actualHeight > maxHeight || actualWidth > maxWidth {
        if imgRatio < maxRatio {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight
            actualWidth = imgRatio * actualWidth
            actualHeight = maxHeight
        }
        else if imgRatio > maxRatio {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth
            actualHeight = imgRatio * actualHeight
            actualWidth = maxWidth
        }
        else {
            actualHeight = maxHeight
            actualWidth = maxWidth
        }
    }
    let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
    UIGraphicsBeginImageContext(rect.size)
    image.draw(in: rect)
    let img = UIGraphicsGetImageFromCurrentImageContext()
    let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality)) ?? Data()
    UIGraphicsEndImageContext()
    return imageData
}
