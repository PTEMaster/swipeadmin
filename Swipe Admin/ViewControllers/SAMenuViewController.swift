//
//  SAMenuViewController.swift
//  Swipe Admin
//
//  Created by mac on 18/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import UIKit

protocol MenuDelegate:NSObject {
    func selectLocationMenu(items:GetLocationModal)
    func selectFilterMenu(filter:FilterModal)
}

class SAMenuViewController: UIViewController {
    
    var locationList = [GetLocationModal]()
    var arrFilterList = [FilterModal]()
    
    weak var delegate:MenuDelegate?
    
    var isFilter:Bool = false
    

    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
}
//MARK:- TableView delegate and dataSource method
extension SAMenuViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFilter {
           return arrFilterList.count
        } else {
         return locationList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell
        
        if isFilter {
            cell?.lblRoomName.text = arrFilterList[indexPath.row].dispalyName ?? ""
        } else {
         cell?.lblRoomName.text = locationList[indexPath.row].RoomName ?? ""
        }
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFilter {
            delegate?.selectFilterMenu(filter: arrFilterList[indexPath.row])
        } else {
         delegate?.selectLocationMenu(items: locationList[indexPath.row])
        }
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- UITableView cell class
class MenuTableViewCell: UITableViewCell {
 @IBOutlet weak var lblRoomName: UILabel!
}


//MARK:- Model class
class FilterModal: NSObject {
    var dispalyName:String?
    var filterName:String?
    init(dispaly:String, filter:String) {
        self.dispalyName = dispaly
        self.filterName = filter
    }
}

extension MenuDelegate {
    func selectLocationMenu(items:GetLocationModal) {}
    func selectFilterMenu(filter:FilterModal) {}
}
