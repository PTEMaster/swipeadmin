//
//  AlertControllerViewController.swift
//  Swipe Admin
//
//  Created by mac on 12/08/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {
    
    @IBOutlet weak var lblStudentId: UILabel!
    @IBOutlet weak var lblStudentName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    
    var strStudentId:String = ""
    var strStudentName:String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        lblStudentName.text = "Student Name :" + strStudentName
        lblStudentId.text = "Student Id :" + strStudentId
        // Do any additional setup after loading the view.
        let currentTime = Date()
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "hh:mm:ss"
        let strTime = dateFormatter.string(from: currentTime)
        lblTime.text = "Time :" + strTime
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
