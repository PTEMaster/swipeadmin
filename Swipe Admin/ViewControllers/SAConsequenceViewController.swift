//
//  SAConsequenceViewController.swift
//  Swipe Admin
//
//  Created by mac on 16/04/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import UIKit
import Alamofire
class SAConsequenceViewController: BaseViewController {
    var permissions = String()
    var studentDetail = [String: Any]()
    var consequenceDetail = [String:Any]()
    var arr_Consequence = NSMutableArray()
    @IBOutlet weak var btnSelectConswquence: UIButton!
    @IBOutlet weak var btnAssignConsequence: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        permissions  = (loginModel?.permissions![0])!
        // btnAssignConsequence.isHidden = true
        GetConsequesce_API()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionConsequence(_ sender: UIButton) {
        let alertController = storyboard?.instantiateViewController(withIdentifier: "SAConsTableViewController") as! SAConsTableViewController
        alertController.modalPresentationStyle = .popover
        alertController.delegate = self
        alertController.Arr_Consequence = self.arr_Consequence
        let height = self.view.frame.size.height / 2
        let width = sender.frame.size.width
        alertController.preferredContentSize = CGSize(width: width, height: height)
        let popover = alertController.popoverPresentationController
        popover?.permittedArrowDirections =  .up
        popover?.delegate = self
        popover?.sourceView = sender
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func actionAssignConsequence(_ sender: UIButton) {
        self.assignConsequenceAPI()
    }
    
    func assignConsequenceAPI() {
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }

        let today = Date() // date is then today for this example
        //let days = consequenceDetail["ServeByDays"]  as? Int
       // let tomorrow = today.add(days: days!)
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date  1/17/2018
        formatter.dateFormat = "MM/dd/yyyy"
        //let myString = formatter.string(from: tomorrow)
        let currentdate = formatter.string(from: today)
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
          permissions  = (loginModel?.permissions![0])!
        let postParams: [String: AnyObject] =
            [
                "SchoolId" : permissions as AnyObject,
                "StudentNumber" : studentDetail["StudentNumber"] as AnyObject,
                "AggregateId" : studentDetail["UniqueIdentifier"] as AnyObject,
                "SubmitBy" : loginModel?.userName as AnyObject,
                "Type" : "0" as AnyObject,
                "ServeBy" : currentdate as AnyObject,
                "Text" : consequenceDetail["Name"] as AnyObject,
                "Units" : "1" as AnyObject,
                "DateAdded" : currentdate as AnyObject
        ]
        Networking.performApiCall(Networking.Router.AssignConsequence((postParams as [String : AnyObject]?)!), callerObj: self, showHud:true) { (response) -> () in
            appDelegate.hideHUD(self.view)
            
            if response.result.isSuccess
            {
                if let result = response.result.value {
                     let resultarr = result as! NSDictionary
                    //print("assign consequence=\(result)")
                    Util.showAlertWithMessage(resultarr["responseText"] as! String, title: kAPI_Message)
                }else
                {
                      Util.showAlertWithMessage("Not Assign", title: kAPI_Message)
                }
            }
        }
        
    }
    
    func GetConsequesce_API()  {
        
        let headers: HTTPHeaders = [
            "SessionId": appDelegate.strSessionID,
            "Accept": "application/json"
        ]
        appDelegate.showHUD("", onView: self.view)
        Alamofire.request("\(kAPI_BaseURL)\(permissions)/CorrectiveActions", headers: headers)
            .responseJSON { response in
                appDelegate.hideHUD(self.view)
                print("consequence=\(response)")
                if response.result.isSuccess {
                    if let result = response.result.value {
                        
                        let resultarr = result as! NSDictionary
                        if resultarr.count == 0 {
                           // Util.showAlertWithMessage("No Consequence", title: kAPI_Message)
                        } else {
                            if resultarr["status"] as? Bool == true {
                                if  let DataArray = resultarr["data"] as? NSArray {
                                    self.arr_Consequence = DataArray.mutableCopy() as! NSMutableArray
                                    let consequenceS = self.arr_Consequence.object(at: 0) as! [String:Any]
                                    self.consequenceDetail = consequenceS
                                    let stringAssign = consequenceS["Name"] as? String
                                    self.btnSelectConswquence.setTitle(stringAssign, for: .normal)
                                }
                            } else {
                            // Util.showAlertWithMessage("No Consequence", title: kAPI_Message)
                            }
                           

                        }
                    }
                }
        }
        self.view.endEditing(true)
    }
    
}

//MARK:- UIPopover PresentationController Delegate
extension SAConsequenceViewController:UIPopoverPresentationControllerDelegate , SAConsTableViewDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
         return .none
     }

        
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
         return true
    }
    
    func selectLocationMenu(items: NSDictionary) {
        let stringAssign = items["Name"] as? String
        btnAssignConsequence.isHidden = false
        btnSelectConswquence.setTitle(stringAssign, for: .normal)
        consequenceDetail = items as! [String : Any]
    }
 
}
