//
//  Networking.swift
//  Bosala


import UIKit
import AVFoundation
import MBProgressHUD
import SwiftyJSON
import Alamofire

struct Networking {
    
    enum Router: URLRequestConvertible {
        
        //****************************************************
        // MARK: - Register
        //****************************************************
        
        case signUpBothSide([String: AnyObject])
        
        //****************************************************
        // MARK: - Login/Signup
        //****************************************************
        
        case userLogin([String: AnyObject])
        
        case AssignConsequence([String: AnyObject])
        
        //****************************************************
        // MARK: - Logout
        //****************************************************

        case facebookLogout
        //****************************************************
        // MARK: - get near by provider according to purticular service
        //****************************************************
        case NearbyProvider([String: AnyObject])
        
         case ProviderRequest([String: AnyObject])
        
         case AcceptRequest([String: AnyObject])
        
         case RejectRequest([String: AnyObject])
        
         case PendingRequest([String: AnyObject])
        
       case SendRequest([String: AnyObject])
        
        case appLogout([String: AnyObject])

        //****************************************************
        // MARK: - Service
        //****************************************************
        
        case servicesList
        
        case updateProviderSerTran([String: AnyObject])
        
        case userDetails([String: AnyObject])
        
        case registrationCount
        
        case subServicesList([String: AnyObject])
        
        //****************************************************
        // MARK: - Buyer side
        //****************************************************
        
        //** Forgot Password API
        case forgotPassword([String: AnyObject])
        
        //** Update Profile
        case updateProfile([String: AnyObject])
       
        //** About Us API
        case aboutUs
        
        //** Reset Password API
        case resetPassword([String: AnyObject])
        
        //** Fav-Unfav API
        case favUnfav(String)
        
        case editStaff(String, [String: AnyObject])
       
        //****************************************************
        // MARK: - Seller side
        //****************************************************
      
        //** Edit Sell Property
        case editSellMyProperty(Int, [String: AnyObject])
        
        
        //****************************************************
        // MARK: - Profile
        //****************************************************
        
        //** Profile
        case profile(String)
        
        
        //****************************************************
        // MARK: - Message
        //****************************************************
        
        //** Message Send
        case messageSend(String)
        
        //****************************************************
        // MARK: - Location Master
        //****************************************************
        
        //** location Countries
        case masterCountries
        
        //** location Town
        case masterTown(String)
        
        
        //****************************************************
        // MARK: - Notification
        //****************************************************
        
        //** Delete Notification
        case deleteNotification(String)
        
        //****************************************************
        // MARK: - Methods
        //****************************************************
        
        
        var method: HTTPMethod {
            
            switch self {
            //** Post Api
            case .signUpBothSide, .userLogin, .AssignConsequence, .appLogout, .userDetails, .registrationCount, .subServicesList, .updateProviderSerTran, .forgotPassword,  .resetPassword, .facebookLogout, .favUnfav, .updateProfile, .editSellMyProperty, .messageSend, .NearbyProvider, .SendRequest, .ProviderRequest, .AcceptRequest, .RejectRequest, .PendingRequest:
                return .post
                
            //** GET Api
            case .servicesList, .aboutUs, .profile, .masterCountries, .masterTown :
                return .get
                
            //** PUT Api
            case .editStaff:
                return .put
                
            //** DELETE Api
            case  .deleteNotification:
                return .delete
            }
        }
        
        //** Intialize api path in |path|
        var path: String {
            
            switch self {
                
                //****************************************************
                // MARK: - Register
                //****************************************************
                
            case .signUpBothSide:
                return "signup.php"
                
                //****************************************************
                // MARK: - Login/Signup
                //****************************************************
                
            //** Login
            case .userLogin:
                return "auth"
                
            case .AssignConsequence:
                return "Station/Publish/ConsequenceAssigned"
                
                //****************************************************
                // MARK: - Logout
                //****************************************************
               
            //** Logout
            case .facebookLogout:
                return "login/logout"
                
                  //** near by provider    
            case .NearbyProvider:
                return "nearest_provider.php"
                
            //** ProviderRequest
            case .ProviderRequest:
                return "provider_request.php"
                
            //** AcceptRequest
            case .AcceptRequest:
                return "accept_request.php"
                
            //** RejectRequest
            case .RejectRequest:
                return "cancel_request.php"
                
            //** Pending
            case .PendingRequest:
                return "provider_pending_request.php"
           
            //** AppLogout
            case .appLogout:
                return "logout.php"
                
                //****************************************************
                // MARK: - Services
                //****************************************************
            
            //** Service List
            case .servicesList:
                return "service_list.php"
                
            //** User Details
            case .userDetails:
                return "user_info.php"
                
            //** Registration Count
            case .registrationCount:
                return "user_count.php"
                
                
                //****************************************************
                // MARK: - Buyer side
                //****************************************************
                
            //** Forgot Password
            case.forgotPassword:
                return "user_forgot.php"
    
            //** About us
            case .aboutUs:
                return "aboutus.php"
                
            //** Reset Password
            case .resetPassword:
                return "user_update_password.php"
                
            //** Reset Password
            case .updateProviderSerTran:
                return "update_provider.php"
                
            //** Send request
            case .SendRequest:
                return "send_request.php"
             
            //** Sub Service List
            case .subServicesList:
                return "sub_service_list.php"
           
            //** FavUnfav
            case .favUnfav(let strUrl):
                return "property/mark-favorite/\(strUrl)"
                
            case .editStaff(let staffId, _):
                return "salon/edit-staff?staff_id=\(staffId)"
           
                //****************************************************
                // MARK: - Seller side
                //****************************************************
                
            //** Edit Sell My Property
            case .editSellMyProperty(let strUrl, _):
                return "property/update-sell-property/\(strUrl)"
           
                
                //****************************************************
                // MARK: - Profile
                //****************************************************
                
            //** Get Profile
            case .profile(let strUrl):
                return "user/profile\(strUrl)"
                
            //** Update Profile
            case .updateProfile:
                return "edit_user.php"
                
                //****************************************************
                // MARK: - Message
                //****************************************************
                

            //** Message Send
            case .messageSend(let strUrl):
                return "message\(strUrl)"

            //****************************************************
            // MARK: - Location Master
            //****************************************************
                
            //** location Countries
            case .masterCountries:
                return "master"
                
            //** location Borough
            case .masterTown(let strUrl):
                return "master/town/\(strUrl)"
                
                //****************************************************
                // MARK: - Notification
                //****************************************************
                
            //** Delete Notification
            case .deleteNotification(let strUrl):
                return "notification/delete/\(strUrl)"
            }
        }
        
        
        //** MARK: URLRequestConvertible
        func asURLRequest() throws -> URLRequest {
            
            var strUrl = kAPI_BaseURL + path
            strUrl = Util.encodedURL(strUrl)
            
            let URL = Foundation.URL(string:strUrl)!
            print("API-\(URL)")
            var urlRequest = URLRequest(url: URL as URL)
            urlRequest.httpMethod = method.rawValue
            
//            if let token = LoggedInUser.sharedUser.accessToken {
//                urlRequest.setValue("\(token)", forHTTPHeaderField: "access-token")
//            }
            
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content")
            urlRequest.setValue("keep-alive", forHTTPHeaderField: "Connection")
            
            switch self {
                
            case .signUpBothSide(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .userLogin(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
               // URLRequest.setValue("application/json",
                                //    forHTTPHeaderField: "Content-Type")
                urlRequest.setValue("application/json",
                                    forHTTPHeaderField: "Accept")
                
            case .AssignConsequence(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .userDetails(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .appLogout(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .forgotPassword(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .updateProviderSerTran(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .subServicesList(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
   
            case .resetPassword(let parameters):
                urlRequest =  try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .NearbyProvider(let parameters):
                urlRequest =  try JSONEncoding.default.encode(urlRequest, with: parameters)
            case .ProviderRequest(let parameters):
                urlRequest =  try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .AcceptRequest(let parameters):
                urlRequest =  try JSONEncoding.default.encode(urlRequest, with: parameters)
            
            case .RejectRequest(let parameters):
                urlRequest =  try JSONEncoding.default.encode(urlRequest, with: parameters)
            
            case .PendingRequest(let parameters):
                urlRequest =  try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .SendRequest(let parameters):
                urlRequest =  try JSONEncoding.default.encode(urlRequest, with: parameters)
                
            case .updateProfile(let parameters):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
                
            default:
                break
            }
            return urlRequest
        }
    }
    
    
    /**
     * Genric method use for performing web api request and after getting response callback to caller class.
     *
     * - parameter requestName: A perticular request that define in Router Enum. It may contain request parameter or may not be.
     * - parameter callerObj: Object of class which make api call
     * - parameter showHud: A boolean value that represent is need to display hud for the api or not
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    static func performApiCall(_ requestName: Networking.Router, callerObj: AnyObject, showHud: Bool, completionHandler:@escaping ( (DataResponse<Any>) -> Void)) {
        
        //** Show Hud
        if showHud {
            var hudTitle : String
            hudTitle = "Loading..."
            
            appDelegate.showHUD(hudTitle, onView: (callerObj as! UIViewController).view)
        }
        
        let request = Alamofire.request(requestName).validate().responseJSON { response in
            
           // print("Parsed JSON:=========== \(String(describing: response.result.value))")
           
            
            //** Hide Hud
            if showHud {
                appDelegate.hideHUD((callerObj as! UIViewController).view)
            }
            
            switch response.result {
                
            case .success: break
              //  print()
                //  print("Get Success response from server with status code:\(String(describing: response.response?.statusCode)), for api request:\(String(describing: response.request?.url))")
               
                
            //** Handle failure response
            case .failure:
                print("Get response from server for api request:\(String(describing: response.request?.url)) in failure section")
                
                
                Networking.handleApiResponse(response)
            }
            completionHandler(response)
        }
        
       //  print("Request Added to Queue for exection. Request URL:\(request)")
       
    }
    
    static func handleApiResponse(_ response: DataResponse<Any>) {
        
        let errorCode = response.response?.statusCode
        if errorCode == nil {
            //errorCode = response.result.error?.code
        }
        
       //   print("Get response from server with status code:\(String(describing: errorCode)), for api request:\(String(describing: response.request?.url))")
        
      
        let dataString = String(data: response.data!, encoding: String.Encoding.utf8)
        
        let result = Util.convertStringToDictionary(dataString!)
        
        var errorDescription = ""
        
        if let errorDes = result?["message"] {
            errorDescription = errorDes as! String
        }
        
        if errorDescription == "" && dataString != nil {
            errorDescription = dataString!
        }
        
           print("Api response error:\(errorDescription)")
      
        
        var strError = errorDescription as String
        
        if strError.count > 150 {
            strError = ""
        }
        
        if let contentType = response.response?.allHeaderFields["Content-Type"] as? String {
            if contentType == "text/html" {
                strError = "Server error"
            }
        }
        
        if let httpStatusCode = errorCode {
            switch httpStatusCode {
                
            case 401:
             
                  let uiAlert = UIAlertController(title: "Unauthorized", message: "Invalid UserName or Password" , preferredStyle:UIAlertController.Style.alert)
                appDelegate.window?.rootViewController!.present(uiAlert, animated: true, completion: nil)
                
                uiAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                   
                }))
                
            //** Almofire libarary error code
            case -999:
                  print("\(String(describing: response.request?.url)) request was cancelled")
               
            case -1001:
                Util.showAlertWithMessage(msgTimeOut, title:"Error")
            case -1003, -1004, -1009:
                Util.showAlertWithMessage(msgCheckConnection, title:"Error")
            case -1005:
                Util.showAlertWithMessage(msgConnectionLost, title:"Error")
            case -1200, -1201, -1202, -1203, -1204, -1205, -1206:
                Util.showAlertWithMessage("The secure connection failed for an unknown reason.", title:"SSL Server Error")
                
            default:
                if Util.isValidString(strError) {
                    Util.showAlertWithMessage(strError, title:"Error")
                }
                else {
                    Util.showAlertWithMessage(msgSorry, title:"Error")
                }
            }
        }
        else {
            Util.showAlertWithMessage(msgSorry, title:"Error")
        }
    }
    
    
    /**
     *   This method upload image(s) as a multipart data format
     * - parameter requestName: A perticular request that define in Router Enum. It may contain request parameter or may not be.
     * - parameter imageArray: Array of images it must not be nil
     * - parameter callerObj: Object of class which make api call
     * - parameter showHud: A boolean value that represent is need to display hud for the api or not
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    static func uploadImages(_ requestName: Networking.Router, imageArray: [UIImage], callerObj: AnyObject, showHud: Bool, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        
        if imageArray.count < 1 {
            return
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            var index = 1
            for image in imageArray {
                let imageData: Data = (image.jpegData(compressionQuality: 1.0) as Data?)!
                
                multipartFormData.append(imageData, withName: "home-\(index)", fileName: "home-\(index)", mimeType: "image/jpeg")
                
                index += 1
            }
            }, with: requestName, encodingCompletion: { result in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                         print("Image(s) Uploaded successfully:\(response)")
                       
                        Networking.handleApiResponse(response)
                    }
                case .failure(let encodingError):
                     print("encodingError:\(encodingError)")
                    
                }
                completionHandler!(result)
        })
        
    }
    
    
    /**
     *   This method upload image(s) as a multipart data format
     * - parameter requestName: A perticular request that define in Router Enum. It may contain request parameter or may not be.
     * - parameter imageArray: Array of images it must not be nil
     * - parameter callerObj: Object of class which make api call
     * - parameter showHud: A boolean value that represent is need to display hud for the api or not
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    static func uploadVideos(_ requestName: Networking.Router, videoArray: [URL], callerObj: AnyObject, showHud: Bool, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        
        if videoArray.count < 1 {
            return
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var index = 1
            for assetsUrl in videoArray {
                
                let videoData = NSData(contentsOf: assetsUrl)
                
                if videoData != nil {
                    //multipartFormData.append(videoData!, withName: "videos", fileName: "videos.mov", mimeType: "video/mov")
                    
                    multipartFormData.append(assetsUrl, withName: "videos", fileName: "videos.mov", mimeType: "video/mov")
                }
                index += 1
            }
            }, with: requestName, encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        print("Image(s) Uploaded successfully:\(response)")
                       
                        Networking.handleApiResponse(response)
                    }
                case .failure(let encodingError):
                    print("encodingError:\(encodingError)")
                }
                completionHandler!(encodingResult)
        })
    }
    
    
    /**
     *   This method upload image(s) as a multipart data format
     * - parameter requestName: A perticular request that define in Router Enum. It may contain request parameter or may not be.
     * - parameter imageArray: Array of images it must not be nil
     * - parameter callerObj: Object of class which make api call
     * - parameter showHud: A boolean value that represent is need to display hud for the api or not
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    static func uploadImagesWithParams(_ requestName: Networking.Router, imageArray: [UIImage], strImageKey : String, dictParams: [String: AnyObject], callerObj: AnyObject, showHud: Bool, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        
        if imageArray.count < 1 {
            return
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var index = 1
            for image in imageArray {
                let imageData: NSData? = image.jpegData(compressionQuality: 1.0) as NSData?
                if imageData != nil {
                    
                    multipartFormData.append(imageData! as Data, withName: strImageKey, fileName: "image.jpeg", mimeType: "image/jpeg")
                    
                    for (key, value) in dictParams {
                        let data = "\(value)".data(using: .utf8)
                        multipartFormData.append(data! as Data, withName: key)
                    }
                }
                index += 1
            }
            }, with: requestName,encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        print("Image(s) Uploaded successfully:\(response)")
                        //Networking.handleApiResponse(response)
                    }
                case .failure(let encodingError):
                    print("encodingError:\(encodingError)")
                    // Networking.handleApiResponse(response)
                    
                    Util.showAlertWithMessage(msgSorry, title:"Error")
                }
                completionHandler!(encodingResult)
            }
        )
    }
    
    static func uploadBothImagesWithParams(_ requestName: Networking.Router, imageArray: Dictionary<String, Any>, dictParams: [String: AnyObject], callerObj: AnyObject, showHud: Bool, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        
        if imageArray.count < 1 {
            return
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var index = 1
            
            for  (key, value) in imageArray
            {
                multipartFormData.append((value as! UIImage).jpegData(compressionQuality: 0.9)!, withName: key, fileName: "swift_file.jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in dictParams {
                let data = "\(value)".data(using: .utf8)
                multipartFormData.append(data! as Data, withName: key)
            }
            
//            for image in imageArray {
//                let imageData: NSData? = UIImageJPEGRepresentation(image, 1.0) as NSData?
//                if imageData != nil {
//                    
//                    multipartFormData.append(imageData! as Data, withName: strImageKey, fileName: "image.jpeg", mimeType: "image/jpeg")
//                    
//                    for (key, value) in dictParams {
//                        let data = "\(value)".data(using: .utf8)
//                        multipartFormData.append(data! as Data, withName: key)
//                    }
//                }
//                index += 1
//            }
        }, with: requestName,encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print("Image(s) Uploaded successfully:\(response)")
                    //Networking.handleApiResponse(response)
                }
            case .failure(let encodingError):
                print("encodingError:\(encodingError)")
                // Networking.handleApiResponse(response)
                
                Util.showAlertWithMessage(msgSorry, title:"Error")
            }
            completionHandler!(encodingResult)
        }
        )
    }

    
    
    static func uploadTwoTypeImagesWithParams(_ requestName: Networking.Router, imageArrayOne: [UIImage], imageArrayTwo: [UIImage], strImageKeyOne : String, strImageKeyTwo : String, dictParams: [String: AnyObject], callerObj: AnyObject, showHud: Bool, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        
        var isArrayOneIsGreater = true
        
        if imageArrayOne.count < imageArrayTwo.count {
            isArrayOneIsGreater = false
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            var index = 1
            var indexOne = 1
            
            var isParamsPosted = false
            
            if isArrayOneIsGreater == true {
                
                if imageArrayOne.count == 0 {
                    for (key, value) in dictParams {
                        let data = "\(value)".data(using: .utf8)
                        multipartFormData.append(data! as Data, withName: key)
                    }
                } else {
                    for imageOne in imageArrayOne {
                        
                        var imageDataOne: NSData? = imageOne.jpegData(compressionQuality: 1.0) as NSData?
                        
                        var imageSize = (imageDataOne?.length)! / 1024
                        
                        if imageSize > 9000 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.02) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        else if imageSize > 6000 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.03) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        else if imageSize > 5000 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.05) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        else if imageSize > 4000 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.07) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        else if imageSize > 3000 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.1) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        else if imageSize > 2000 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.15) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        else if imageSize > 1000 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.3) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        else if imageSize > 500 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.4) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        else if imageSize > 400 {
                            imageDataOne = imageOne.jpegData(compressionQuality: 0.5) as NSData?
                            imageSize = (imageDataOne?.length)! / 1024
                        }
                        
                        
                        var imageTwo = UIImage()
                        var imageDataTwo = NSData()
                        
                        var isArrayTwoAvailable = false
                        
                        if index <= imageArrayTwo.count {
                            imageTwo = imageArrayTwo[index - 1]
                            isArrayTwoAvailable = true
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 1.0)! as NSData
                            
                            var imageSize = (imageDataTwo.length) / 1024
                            
                            if imageSize > 9000 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.02) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                            else if imageSize > 6000 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.03) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                            else if imageSize > 5000 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.05) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                            else if imageSize > 4000 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.07) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                            else if imageSize > 3000 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.1) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                            else if imageSize > 2000 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.15) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                            else if imageSize > 1000 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.3) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                            else if imageSize > 500 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.4) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                            else if imageSize > 400 {
                                imageDataTwo = (imageTwo.jpegData(compressionQuality: 0.5) as NSData?)!
                                imageSize = (imageDataTwo.length) / 1024
                            }
                        }
                        
                        if imageDataOne != nil {
                            
                            //multipartFormData.appendBodyPart(data: imageDataOne!, name: strImageKeyOne, fileName: "image.jpeg", mimeType: "image/jpeg")
                            multipartFormData.append(imageDataOne! as Data, withName: strImageKeyOne, fileName: "image_\(index - 1).jpeg", mimeType: "image/jpeg")
                            
                            //appDelegate.arrOderHomeImages["image_\(index - 1).jpeg"] = "\(index - 1)"
                            
                            //print(appDelegate.arrOderHomeImages)
                            
                            if isArrayTwoAvailable == true {
                                //multipartFormData.appendBodyPart(data: imageDataTwo, name: strImageKeyTwo, fileName: "image.jpeg", mimeType: "image/jpeg")
                                multipartFormData.append(imageDataTwo as Data, withName: strImageKeyTwo, fileName: "image_\(index - 1).jpeg", mimeType: "image/jpeg")
                                
                                //appDelegate.arrOrderFloorplanImages["image_\(index - 1).jpeg"] = "\(index - 1)"
                                //appDelegate.arrOrderFloorplanImages.add(valueOne)
                                //print(appDelegate.arrOrderFloorplanImages)
                            }
                            
                            if isParamsPosted == false {
                                
                                for (key, value) in dictParams {
                                    let data = "\(value)".data(using: .utf8)
                                    multipartFormData.append(data! as Data, withName: key)
                                }
                                
                                isParamsPosted = true
                            }
                        }
                        index += 1
                    }
                }
            }
            else {
                
                if imageArrayTwo.count == 0 {
                    for (key, value) in dictParams {
                        
                        let data = "\(value)".data(using: .utf8)
                        multipartFormData.append(data! as Data, withName: key)
                    }
                } else {
                    for imageTwo in imageArrayTwo {
                        
                        var imageDataTwo: NSData? = imageTwo.jpegData(compressionQuality: 1.0) as NSData?
                        
                        var imageSize = (imageDataTwo?.length)! / 1024
                        
                        if imageSize > 9000 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.02) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        else if imageSize > 6000 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.03) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        else if imageSize > 5000 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.05) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        else if imageSize > 4000 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.07) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        else if imageSize > 3000 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.1) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        else if imageSize > 2000 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.15) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        else if imageSize > 1000 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.3) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        else if imageSize > 500 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.4) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        else if imageSize > 400 {
                            imageDataTwo = imageTwo.jpegData(compressionQuality: 0.5) as NSData?
                            imageSize = (imageDataTwo?.length)! / 1024
                        }
                        
                        var imageOne = UIImage()
                        var imageDataOne = NSData()
                        
                        var isArrayOneAvailable = false
                        
                        if indexOne <= imageArrayOne.count {
                            imageOne = imageArrayOne[indexOne - 1]
                            isArrayOneAvailable = true
                            imageDataOne = imageOne.jpegData(compressionQuality: 1.0)! as NSData
                            
                            var imageSize = (imageDataOne.length) / 1024
                            
                            if imageSize > 9000 {
                                imageDataOne = (imageOne.jpegData(compressionQuality: 0.02) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            } else if imageSize > 6000 {
                                imageDataOne = (imageOne.jpegData(compressionQuality: 0.03) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            }
                            else if imageSize > 5000 {
                                imageDataOne = (imageOne.jpegData(compressionQuality: 0.05) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            }
                            else if imageSize > 4000 {
                                imageDataOne = (imageOne.jpegData(compressionQuality: 0.07) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            }
                            else if imageSize > 3000 {
                                imageDataOne = (imageOne.jpegData(compressionQuality: 0.1) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            }
                            else if imageSize > 2000 {
                                imageDataOne = (imageOne.jpegData(compressionQuality: 0.15) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            }
                            else if imageSize > 1000 {
                                imageDataOne = (imageTwo.jpegData(compressionQuality: 0.3) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            }
                            else if imageSize > 500 {
                                imageDataOne = (imageTwo.jpegData(compressionQuality: 0.4) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            }
                            else if imageSize > 400 {
                                imageDataOne = (imageTwo.jpegData(compressionQuality: 0.5) as NSData?)!
                                imageSize = (imageDataOne.length) / 1024
                            }
                        }
                        
                        if imageDataTwo != nil {
                            
                            //multipartFormData.appendBodyPart(data: imageDataTwo!, name: strImageKeyTwo, fileName: "image.jpeg", mimeType: "image/jpeg")
                            multipartFormData.append(imageDataTwo! as Data, withName: strImageKeyTwo, fileName: "image_\(indexOne - 1).jpeg", mimeType: "image/jpeg")
                            //appDelegate.arrOrderFloorplanImages["image_\(indexOne - 1).jpeg"] = "\(indexOne - 1)"
                            //appDelegate.arrOrderFloorplanImages.add(valueOne)
                            //print(appDelegate.arrOrderFloorplanImages)
                            
                            if isArrayOneAvailable == true {
                                //multipartFormData.appendBodyPart(data: imageDataOne, name: strImageKeyOne, fileName: "image.jpeg", mimeType: "image/jpeg")
                                multipartFormData.append(imageDataOne as Data, withName: strImageKeyOne, fileName: "image_\(indexOne - 1).jpeg", mimeType: "image/jpeg")
                                
                                //appDelegate.arrOderHomeImages["image_\(indexOne - 1).jpeg"] = "\(indexOne - 1)"
                                //appDelegate.arrOderHomeImages.add(value)
                                //print(appDelegate.arrOderHomeImages)
                            }
                            
                            if isParamsPosted == false {
                                
                                for (key, value) in dictParams {
                                    
                                    let data = "\(value)".data(using: .utf8)
                                    multipartFormData.append(data! as Data, withName: key)
                                }
                                
                                isParamsPosted = true
                            }
                        }
                        indexOne += 1
                    }
                }
                
                
            }
            }, with: requestName, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        print("Image(s) Uploaded successfully:\(response)")
                        
                        //Networking.handleApiResponse(response)
                    }
                case .failure(let encodingError):
                 
                      print("encodingError:\(encodingError)")
                    // Networking.handleApiResponse(response)
                    
                    Util.showAlertWithMessage(msgSorry, title:"Error")
                }
                completionHandler!(encodingResult)
            }
        )
    }
    
    
    static func uploadVideoWithParams(_ requestName: Networking.Router, imageArray: [URL], strImageKey : String, dictParams: [String: AnyObject], callerObj: AnyObject, showHud: Bool, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        
        if imageArray.count < 1 {
            return
        }
        
        // TODO: Need to check
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            var index = 1
            for image in imageArray {
                
                let videoData = NSData(contentsOf: image)
                //let imageData: NSData? = UIImagePickerControllerReferenceURL(image, 1.0)
                if videoData != nil {
                    
                    multipartFormData.append(videoData! as Data, withName: strImageKey, fileName: "video.mov", mimeType: "video/mov")
                            
                    for (key, value) in dictParams {
                        let data = "\(value)".data(using: .utf8)
                        multipartFormData.append(data! as Data, withName: key)
                    }
                }
                index += 1
            }
            }, with: requestName, encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                
                                upload.responseJSON { response in
                                    
                                      print("Image(s) Uploaded successfully:\(response)")
                                   
                                }
                            case .failure(let encodingError):
                                   print("encodingError:\(encodingError)")
                                // Networking.handleApiResponse(response)
                            }
                            completionHandler!(encodingResult)
            }
        )
    }  
}


/**
 * Response Object Serialization Extension
 */
public protocol ResponseObjectSerializable {
    init?(response: HTTPURLResponse, representation: AnyObject)
}


