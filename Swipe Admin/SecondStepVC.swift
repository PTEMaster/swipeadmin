//
//  FirstStepVC.swift
//  SwipeK Teacher
//
//  Created by mac on 22/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SecondStepVC: UIViewController {

    
    @IBOutlet weak var lblStudent: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var ImgQRCode: UIImageView!
    var studentDetail = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
print(studentDetail)
        lblDate.text = Date.getCurrentDate()
        let StudentNumber2 = studentDetail["StudentNumber"] as? String
      /*  if let strOut = UserDefaults.standard.string(forKey: "person") {
            let strBarCode = "\(strOut)"
            self.ImgQRCode.image = self.generateBarcode(from: strBarCode, nameBarCode: "CIQRCodeGenerator")
        }*/
        self.ImgQRCode.image = self.generateBarcode(from: StudentNumber2 ?? "", nameBarCode: "CIQRCodeGenerator")
        
        if let displayName = UserDefaults.standard.string(forKey: "displayName") {
            lblStudent.text = "\(displayName)"
            
        }
        
    }

    @IBAction func actionYes(_ sender: Any) {
        appDelegate.isLoginTrue = true
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SAStudentStaffList_VC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }



        
      //  let vc  = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
      //  self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // MARK: - QR Code
    
       func generateBarcode(from string: String , nameBarCode:String) -> UIImage? {
         let data = string.data(using: String.Encoding.ascii)

         if let filter = CIFilter(name: nameBarCode) {
             filter.setValue(data, forKey: "inputMessage")
            //filter.setValue("H", forKey: "inputCorrectionLevel")
             let transform = CGAffineTransform(scaleX: 10, y: 10)

             if let output = filter.outputImage?.transformed(by: transform) {
                 return UIImage(ciImage: output)
             }
         }

         return nil
     }
    
    func fromString(string : String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let output = filter?.outputImage?.transformed(by: transform)
        return UIImage(ciImage: output!)
    }
    
   
    
}



extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "MM/dd/yy"

        return dateFormatter.string(from: Date())

    }
}
