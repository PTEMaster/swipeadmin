//
//  AppDelegate.swift
//  Swipe Admin
//
//  Created by CTInformatics on 12/01/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Reachability
import IQKeyboardManagerSwift
import SwiftyJSON
import MBProgressHUD
import Firebase


///jf.Hamro-Nepali-Music
func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var isNetworkBecomeAvailableFirstTime = false
    var reachability: Reachability?
      var strDeviceToken: String = ""
     var strSessionID: String = ""
     var strPermission: String = ""
    var StrUsername: String = ""
    var isLoginTrue : Bool = false
// client identifer :- com.swipek12.mobileadmin
    // cti indentifier :-  cti.swipeadmin.com

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Thread.sleep(forTimeInterval: 2)
        //** Configure |IQKeyboardManager|
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 150
        UIApplication.shared.applicationIconBadgeNumber = 0
   
        // Start reachability without a hostname intially
        setupReachability(nil, useClosures: false)
        startNotifier()
        FirebaseApp.configure()
//        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)

        // Override point for customization after application launch.
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        
        let reachability = hostName == nil ? try? Reachability() : try? Reachability(hostname: hostName!)
        self.reachability = reachability
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.reachabilityChanged(_:)), name: Notification.Name.reachabilityChanged, object: reachability)
    }
    
    func startNotifier() {
        do {
            try reachability?.startNotifier()
        }
        catch {
            return
        }
    }
    
    
    
    func stopNotifier() {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: nil)
        reachability = nil
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        isNetworkAvailable = reachability.isReachable
        
        if reachability.isReachable {
            
//            if UserDefaults.standard.bool(forKey: Key_UD_IsUserLoggedIn) && isNetworkAvailable && !isNetworkBecomeAvailableFirstTime {
                
                isNetworkBecomeAvailableFirstTime = true
                
                //** Post notification for |NetworkBecomeAvailableFirstTime|
             //   NotificationCenter.default.post(name: Notification.Name(rawValue: notificationNetworkBecomeAvailableFirstTime), object: self, userInfo:nil)
           // }
        }
        else {
            if isNetworkBecomeAvailableFirstTime == false {
               // NotificationCenter.default.post(name: Notification.Name(rawValue: notificationNetworkBecomeAvailableFirstTime), object: self, userInfo:nil)
            }
        }
    }
    
    //MARK :- Mbpogresshud methods
    func showHUD(_ hudTitle : String, onView: UIView) {
        
        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: onView, animated: true)
        //hud.mode = MBProgressHUDMode.AnnularDeterminate
        hud.bezelView.color = UIColor.systemBlue
        hud.contentColor = UIColor.orange
        hud.bezelView.style = .solidColor
        
        hud.label.textColor = colorWhite
        hud.label.text = "Please wait"
        hud.bezelView.color = UIColor.darkGray
        hud.label.font = UIFont(name: AppFontBook, size: 17)
        
        if Util.isValidString(hudTitle) {
            hud.label.text = hudTitle
        }
        else {
            hud.label.text = "Loading..."
        }
    }
    
    func hideHUD(_ fromView: UIView) {
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    

}

