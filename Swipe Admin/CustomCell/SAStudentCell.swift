//
//  SAStudentCell.swift
//  Swipe Admin
//
//  Created by mac on 18/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import UIKit

class SAStudentCell: UITableViewCell {
     @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblID:UILabel!
    @IBOutlet weak var lblHR:UILabel!
    @IBOutlet weak var lblGR:UILabel!
    @IBOutlet weak var btnnext:UIButton!
    @IBOutlet weak var imgPhoto:UIImageView!
    @IBOutlet weak var viewbackground: UIView!
    //SheduleData
        @IBOutlet weak var lblSClass:UILabel!
        @IBOutlet weak var lblSRoom:UILabel!
        @IBOutlet weak var lblSPeriod:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
