//
//  SAStudentStaffList_VC.swift
//  Swipe Admin
//
//  Created by CTInformatics on 12/01/18.
//  Copyright © 2018 CT. All rights reserved.
//  SAScanningList_VC

import UIKit
import Alamofire
import NetworkExtension
import SDWebImage
import AVFoundation
import AudioToolbox



class SAScanningList_VC: BaseViewController {
    @IBOutlet weak var tbllist:UITableView!
    @IBOutlet weak var viewNoValue:UIView!
    @IBOutlet weak var txtsearch:UITextField!
    @IBOutlet weak var viewtextsearch:UIView!
    var menuListViewController: SideMenu_VC?
    @IBOutlet var ViewPop: UIView!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    
    var StudentNumber = ""
    var strSearch = ""
    var Arr_StudentData = NSMutableArray()
    var player: AVAudioPlayer??
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let sb = UIStoryboard(name: "Main", bundle: nil)
        menuListViewController = (sb.instantiateViewController(withIdentifier: "SideMenu_VC") as! SideMenu_VC)
      //  menuListViewController!.delegate = self
        txtsearch.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        tbllist.isHidden=true
        viewtextsearch.layer.cornerRadius = viewtextsearch.frame.height/2
        viewtextsearch.layer.borderWidth = 1
        viewtextsearch.clipsToBounds = true
        viewtextsearch.layer.borderColor = UIColor.white.cgColor
        //add obeserver to check device orientation
        tbllist.tableFooterView = UIView()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        txtsearch.delegate = self
        tbllist.register(UINib(nibName: "SAStudentCell", bundle: Bundle.main), forCellReuseIdentifier: "SAStudentCell")
        // Do any additional setup after loading the view.
        
    }
   
 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tbllist.reloadData()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        /*self.navigationController?.view.addSubview(self.menuListViewController!.view)
        self.setupSideMenuAnimations(isAdd: true, toView:  self.menuListViewController!.view)*/
    }
     override func viewDidAppear(_ animated: Bool) {
        
        txtsearch.attributedPlaceholder = NSAttributedString(string: "Student/Staff",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        print(txtsearch.text)
        if CanSearch() {
            SearchAPI()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
       appDelegate.hideHUD(self.view)
    }
    
    @IBAction func actionSideMenu(_ sender: Any) {
      //  self.navigationController?.view.addSubview( self.menuListViewController!.view)
        //      self.setupSideMenuAnimations(isAdd: true, toView: self.menuListViewController!.view)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func ClickSearch(sender:Any){
        self.view.endEditing(true)
        if CanSearch() {
            SearchAPI()
        }
    }
    
    func setupSideMenuAnimations(isAdd:Bool, toView:UIView) {
         if isAdd {
            toView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: self.view.frame.size.height)
             
            toView.frame.origin.y =  (self.view.frame.origin.y)
             toView.frame.origin.x = -self.view.frame.width
             toView.alpha = 0
             UIView.animate(withDuration: 1.0) {
                 toView.frame.origin.x = 0
                 toView.alpha = 1
                self.view.addSubview(toView)
             }
         }
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - API methods
    func SearchAPI()  {
        
        appDelegate.showHUD("", onView: self.view)
        let headers: HTTPHeaders = [
            "SessionId": appDelegate.strSessionID,
            "Accept": "application/json"
        ]
        print("headers-\(headers)-")
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        Alamofire.request("\(kAPI_BaseURL)\((loginModel?.permissions![0])!)/Students/Find?criteria=\(txtsearch.text!)", headers: headers)
            .responseJSON { response in
         
                if response.result.isSuccess
                {
                  //  print("Response-\(response.result.value!)-")
                    if let result = response.result.value {
                        
                        let resultarr = result as! NSArray
                        if resultarr.count == 0
                        {
                            self.tbllist.isHidden = true
                            self.viewNoValue.isHidden = false
                          Util.showAlertWithMessage("No Student available with name/Id you entered", title: kAPI_Message)
                        }else
                        {
                            self.tbllist.isHidden=false
                            self.viewNoValue.isHidden=true
                            self.Arr_StudentData = resultarr.mutableCopy() as! NSMutableArray
                            self.tbllist.reloadData()
                        }
                    }
        }
        
    appDelegate.hideHUD(self.view)
    }
        //self.view.endEditing(true)
        
    }
    // MARK: - Private methods
    func CanSearch() -> Bool {
        if (txtsearch.text?.isEmpty)! {
           // Util.showAlertWithMessage("Please enter username", title: kAPI_Message)
         Util.showAlertWithMessage("Please insert Name/Id to search", title: kAPI_Message)
            return false
        }
        return true
    }

}

//MARK: - Tableview delegates
extension SAScanningList_VC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arr_StudentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SAStudentCell") as! SAStudentCell
        let obj = self.Arr_StudentData.object(at: indexPath.row) as! [String : Any]
       
        cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.height/2
        cell.imgPhoto.layer.borderWidth = 2
        cell.imgPhoto.clipsToBounds = true
        cell.imgPhoto.layer.borderColor = UIColorFromRGB(rgbValue: 0x28a8f9).cgColor
       
        let strimageurl = "https://" + (obj["imageHost"] as? String ?? "")  + (obj["imageUrl"] as? String ?? "")
  //    print("strimageurl-\(strimageurl)-")
        cell.imgPhoto.sd_setImage(with: URL(string: strimageurl), placeholderImage: UIImage(named: "profile"), options: .queryDiskDataSync, progress: .none, completed: nil)
        
        let strfname = obj["FirstName"] as? String
        let strlname = obj["LastName"] as? String
        cell.lblName.text = "\(strfname ?? "") \(strlname ?? "")"
        cell.lblName.textColor = UIColorFromRGB(rgbValue: 0x28a8f9)
        if let grade = obj["Grade"] as? String {
            cell.lblGR.text = grade
        } else  {
            cell.lblGR.text = "0"
        }
        
        if let homeroom = obj["Homeroom"]
        {
            cell.lblHR.text = homeroom as? String;
        }else{
            cell.lblHR.text = "";
        }
        cell.lblID.text = "\(obj["StudentNumber"]! as Any)"
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 1.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.dropShadow()
        cell.btnnext.layer.cornerRadius = cell.btnnext.frame.height/2
        cell.btnnext.clipsToBounds = true
        cell.btnnext.tag = indexPath.row
        cell.btnnext.addTarget(self, action: #selector(actionStudentDetail), for: .touchUpInside)
        return cell
    }
    
    @objc func actionStudentDetail(sender:UIButton) {
        /* let indexPath = sender.tag
        let obj = self.Arr_StudentData.object(at: indexPath) as! [String : Any]
        if indexPath == 0{
            self.ViewPop.backgroundColor = .green
            self.setupSoundValidandInvalid(sound: "validId")
            self.setupPopView(status: true, toView: self.ViewPop)
        }else{
            self.ViewPop.backgroundColor = .systemRed
            self.setupSoundValidandInvalid(sound: "invalidId")
            self.setupPopView(status: true, toView: self.ViewPop)
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        }*/
    }
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.Arr_StudentData.object(at: indexPath.row) as! [String : Any]
        print(obj)
        
        let strfname = obj["FirstName"] as? String
        let strlname = obj["LastName"] as? String
        StudentNumber = obj["StudentNumber"] as? String ?? ""
        
        lblMsg.text = "\(strfname ?? "") \(strlname ?? "")"
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let currentTime = dateFormatter.string(from: date)
        self.lblTime.text = "\(currentTime)"
        
        
        if obj["Grade"]as! String == "STAFF"{
            let ID =  obj["PersonId"] as? Int ?? 0
            select2(userType: "STAFF", idEmail:  String(ID))
        }else{
            let ID =  obj["PersonId"] as? Int ?? 0
          select2(userType: "STUDENT", idEmail: String(ID))
        }
        
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "identifierstudentDetails" {
            let vc = segue.destination as? SAStudentDetail_VC
            vc?.studentDetail = sender as! [String:Any]
        }
    }
    func setupSoundValidandInvalid(sound:String) {
        let url = Bundle.main.url(forResource: sound, withExtension: "mp3")!
        do {
            //Preparation to play
            do {
                
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
                do {
                    try AVAudioSession.sharedInstance().setActive(true)
                    //print("AVAudioSession is Active")
                } catch _ as NSError {
                    //print(error.localizedDescription)
                }
            } catch let error {
                print("Error in AVAudio Session\(error.localizedDescription)")
            }
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player?.prepareToPlay()
            player?.play()
            
        } catch let error as NSError {
            print(error.description)
        }
    }
    func setupPopView(status:Bool, toView:UIView) {
           toView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60)
             toView.frame.origin.y = self.view.frame.height
             self.view.addSubview(toView)
             UIView.animate(withDuration: 0.5) {
                 toView.frame.origin.y = (self.view.frame.height - (toView.frame.height)) - 60
              // print(toView.frame)
             }
             
             DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                 UIView.animate(withDuration: 0.5, delay: 0.2, options: [.curveEaseInOut], animations: {
                     // self.statusCheck = true
                 }) { (bool) in
                     toView.removeFromSuperview()
                 }
             }
         }
    //MARK:- Button actions
    @IBAction func actionMaual(_ sender: Any) {
     //   self.viewSacn.isHidden = true
      NotificationCenter.default.post(name: SAScanViewController.notificationName, object: nil, userInfo:["data":false])
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "SALocationScanViewControllerListing") as! SALocationScanViewControllerListing
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SALocationScanViewControllerListing") as! SALocationScanViewControllerListing
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func actionScanMode(_ sender: Any) {
         //self.viewManual.isHidden = true
        //self.viewSacn.isHidden = false
        NotificationCenter.default.post(name: SAScanViewController.notificationName, object: nil, userInfo:["data":true])
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "SALocationScanViewControllerScan") as! SALocationScanViewControllerScan
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .coverVertical
            vc.comeFromScan = true
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SALocationScanViewControllerScan") as! SALocationScanViewControllerScan
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .coverVertical
            vc.comeFromScan = true
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}

extension SAScanningList_VC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if CanSearch() {
            SearchAPI()
        }
    }
    

    func select2(userType: String , idEmail: String){
        appDelegate.showHUD("", onView: self.view)
        var params = [String: Any]()
      /*  if userType == "STAFF"{
            params = ["StaffEmail":idEmail]
        }else{
            params = ["StudentId":idEmail]
        }*/
        params = ["PersonId":idEmail]
        
        print(params)
        let url = "\(kAPI_BaseURL)CheckSurveySubmission/"
        print(url)
       POSTrowValue(url: url, param: params, header: nil, isLoader: false)
    }
    
    
    func POSTrowValue(url: String, param: Parameters, header: HTTPHeaders?,isLoader: Bool){
       Alamofire.request(url, method:.post, parameters: param,encoding: JSONEncoding.default, headers: header) .responseJSON { response in
        appDelegate.hideHUD(self.view)
        if response.result.isSuccess{
            if let result = response.result.value {
                print(result)
                if let resultarr = result as? NSArray  {
                    
                    print(resultarr.lastObject)
                   
                    if resultarr.count == 0 {
                       print(resultarr)
                        self.ViewPop.backgroundColor = .systemRed
                        self.setupSoundValidandInvalid(sound: "invalidId")
                        self.setupPopView(status: true, toView: self.ViewPop)
                        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                       
                       // Swipe_Admin.show(message: "Something went wrong")
                        
                    } else {
                        let dict = resultarr.lastObject as? NSDictionary
                        let surveyResult =  dict?["surveyResult"] as? String ?? ""
                        if surveyResult  == "Fail"{
                            self.ViewPop.backgroundColor = .systemRed
                            self.setupSoundValidandInvalid(sound: "invalidId")
                            self.setupPopView(status: true, toView: self.ViewPop)
                            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                        }else{
                            self.isSubmitWebScanApiCall(studentId: self.StudentNumber)
                        }
                        
                        
                    }
                }
            }
        }else{
            print(response.result.error?.localizedDescription)
        }
       }
   }
    
    func isSubmitWebScanApiCall(studentId:String) {
        let url = "\(kAPI_BaseURL)Attendance/SubmitWebScan"
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        let permissions = (loginModel?.permissions![0])!
        var params = [String:Any]()
        params["delimitedStudentString"] =  studentId
        params["schoolId"] = permissions
        params["scanLocation"] = "Covid Scanning"
        params["classLocationScanned"] = false
        params["commandedBy"] = UserDefaults.standard.getUserName()
       // print(params)
        let headers: HTTPHeaders = ["SessionId": appDelegate.strSessionID,
                                    "Accept": "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        //    print(response)
            if response.result.isSuccess {
                if let result = response.result.value {
                   // print(result)
                   /* let date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "HH:mm"
                    let currentTime = dateFormatter.string(from: date)
                    self.lblTime.text = "\(currentTime)"
                    self.ViewPop.backgroundColor = .green
                    self.setupSoundValidandInvalid(sound: "validId")
                    self.setupPopView(status: true, toView: self.ViewPop)*/
                    
                    
                    self.ViewPop.backgroundColor = .green
                    self.setupSoundValidandInvalid(sound: "validId")
                    self.setupPopView(status: true, toView: self.ViewPop)
                    
                }
            }
        }
    }
    
}
