//
//  SideMenu_VC.swift
//  Swipe Admin
//
//  Created by mac on 14/02/18.
//  Copyright © 2018 SwipeK12. All rights reserved.
//

import UIKit
//import SideMenu

protocol SidemenuDelegate:NSObject {
    func didSelectRowAt(indexPath:IndexPath)
}

class SideMenu_VC: BaseViewController {
    weak var delegate:SidemenuDelegate?
let def = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        guard SideMenuManager.default.menuBlurEffectStyle == nil else {
            return
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Click_Logout(sender:Any)
    {
        def.set("None", forKey: "Permissions")
    }
    @IBAction func actionClose(_ sender: Any) {
      self.setupSideMenuAnimation(isAdd: false, toView: self.view)
    }
    
    
    
    func showLoginScreen() {
       // let navigationController = appDelegate.window?.rootViewController as! UINavigationController
     //   for loginVC in (navigationController.viewControllers) {
        //    if (loginVC is ViewController) {
          //      navigationController.popToViewController(loginVC, animated: true)
         //       return
         //   }
       // }
        
       // let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
      //  let rootController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController")
      //  navigationController.pushViewController(rootController, animated: true)
    }

}

extension SideMenu_VC : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey")
        if Int(LoginSurvey!) ?? 0 > 0{
            return 4
        }else{
            return 2
            }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey")
        if Int(LoginSurvey!) ?? 0 > 0{
            
            if indexPath.row == 0 {
                cell.lblTitle.text = "Location Scanning"
                cell.imgICons.image = UIImage(named: "scan")
            }else if indexPath.row == 1{
                cell.lblTitle.text = "Covid Questionaire"
                cell.imgICons.image = UIImage(named: "scan")
            } else if indexPath.row == 2{
                cell.lblTitle.text = "Covid Scanning"
                cell.imgICons.image = UIImage(named: "scan")
            }
            else {
                    cell.lblTitle.text = "Logout"
                    cell.imgICons.image = UIImage(named: "logout_icon")
                }
        }else{
            if indexPath.row == 0{
                cell.lblTitle.text = "Location Scanning"
                cell.imgICons.image = UIImage(named: "scan")
            }else {
                cell.lblTitle.text = "Logout"
                cell.imgICons.image = UIImage(named: "logout_icon")
            }
        }
      
         return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectRowAt(indexPath: indexPath)

        self.setupSideMenuAnimation(isAdd: false, toView: self.view)
    }
}

class SideMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgICons: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
}
