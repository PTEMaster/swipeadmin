//
//  APIManager.swift
//  SwipeK Teacher
//
//  Created by mac on 10/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Alamofire

protocol ApiManagerDelegate:NSObject {
    func successResponse(response:Any, check:String)
    func errorResponse(error:Any, check:String)
}

class APIManager: NSObject {
    static let share = APIManager()
    weak var delegate:ApiManagerDelegate?
    let Connection = "keep-alive"
    
    func performPostJSON(dictData:Data, ApiAction:String) {
        guard NetworkReachabilityManager()!.isReachable else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
            DispatchQueue.main.async(execute: {
                self.delegate?.errorResponse(error: error , check: ApiAction)
            })
            return
        }
        let postUrl =  ApiAction
        print("API==\(postUrl)")
         let urlString = postUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        var postRequest = URLRequest(url: URL(string: urlString ?? "")!, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: 60.0)
        postRequest.httpMethod = "POST"
        postRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //postRequest.setValue("Content-Length", forHTTPHeaderField: "length")
       // postRequest.setValue(Connection, forHTTPHeaderField: "Connection")
        postRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        postRequest.httpBody = dictData
        setupURLSession(postRequest, ApiAction)
    }
    func performPostAPI(dict:[String:Any], APIAction:String) {
       
        guard NetworkReachabilityManager()!.isReachable else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
             DispatchQueue.main.async(execute: {
                self.delegate?.errorResponse(error: error , check: APIAction)
            })
            return
        }
        
        let postUrl =  APIAction
        print("API==\(postUrl)")
        guard let url = URL(string: postUrl) else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your url and try again.error code eferr002"])
                delegate?.errorResponse(error: error , check: APIAction)
            return
        }
        var postRequest = URLRequest(url: url , cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: 60.0)
        postRequest.httpMethod = "POST"
        postRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        postRequest.setValue("Content-Length", forHTTPHeaderField: "length")
        postRequest.setValue(Connection, forHTTPHeaderField: "Connection")
        //postRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let parameters: [String: Any] = dict
        do {
           let jsonParams = try JSONSerialization.data(withJSONObject: parameters, options: [])
           postRequest.httpBody = jsonParams
        } catch { print("Error: unable to add parameters to POST request.")}

        setupURLSession(postRequest, APIAction)
    }
    
    func performGetAPI(dict:[String:Any], APIAction:String) {
        guard NetworkReachabilityManager()!.isReachable  else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your url and try again.error code eferr002"])
            delegate?.errorResponse(error: error , check: APIAction)
            return
        }
        let getUrl = APIAction
        guard let url = URL(string: getUrl) else {
            let error = "Please check your url and try again.error code eferr002"
            delegate?.errorResponse(error: error , check: APIAction)
            return
        }
       
        print("API==\(getUrl)")
        var postRequest = URLRequest(url: url , cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: 60.0)
        postRequest.httpMethod = "GET"
        postRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        postRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        postRequest.setValue(Connection, forHTTPHeaderField: "Connection")
        
        setupURLSession(postRequest, APIAction)
    }
    
 
    
    fileprivate func setupURLSession(_ postRequest: URLRequest, _ Api: String) {
        URLSession.shared.dataTask(with: postRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print("POST Request: Communication error: \(error!)")
                DispatchQueue.main.async {
                    self.delegate?.errorResponse(error: error!, check: Api)
                }
            }
 
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                DispatchQueue.main.async {
                    if data != nil {
                    self.delegate?.errorResponse(error: data!, check: Api)
                      }
                    }
                return
            }
         
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async {
                    self.delegate?.successResponse(response: data as Any, check: Api)
                        print("Response from POST  :\n\(resultObject)")
                    }
                } catch {
                    DispatchQueue.main.async {
                        print("Unable to parse JSON response")
                        self.delegate?.errorResponse(error:error , check: Api)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    print("Error empty response.")
                    self.delegate?.errorResponse(error: error!, check: Api)
                }
            }
        }).resume()
    }
}
