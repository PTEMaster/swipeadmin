//
//  BaseViewController.swift
//  SwipeK Teacher
//
//  Created by mac on 05/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    fileprivate func setupNavigationTitle() {
        // Do any additional setup after loading the view.
      /*  let view = UIView(frame: CGRect(x: 0, y: 0, width: 170, height: 40))
        view.backgroundColor = UIColor(named: "navigationTitleBackgroundColor")
        let btnTitle  = UIButton(type: .system)
        btnTitle.frame = CGRect(x: 10, y: 8, width: 150, height: 28)  // Size
        btnTitle.backgroundColor = UIColor.clear
        btnTitle.setTitle("Swipe Teacher App", for: .normal)
        btnTitle.setTitleColor(.white, for: .normal)
        btnTitle.isUserInteractionEnabled = false
        guard let customFont = UIFont(name: "JosefinSans-light", size: 18) else {
            fatalError("""
                Failed to load the "CustomFont-Light" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
                """
            )
        }
        btnTitle.titleLabel!.font = UIFontMetrics.default.scaledFont(for: customFont)
       // btnTitle.titleLabel!.font = UIFont.boldSystemFont(ofSize: 16)
        view.addSubview(btnTitle)
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        let barButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.rightBarButtonItem = barButtonItem */
        
        // Do any additional setup after loading the view.
        if let navigationBar = self.navigationController?.navigationBar {
            let gradient = CAGradientLayer()
            var bounds = navigationBar.bounds
            bounds.size.height += UIApplication.shared.statusBarFrame.size.height
            gradient.frame = bounds
            gradient.colors = [UIColor(named: "firstColor")?.cgColor ?? UIColor.green , UIColor(named: "thirdColor")?.cgColor ?? UIColor.orange]
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            if let image = getImageFrom(gradientLayer: gradient) {
                navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
            }
        }
        
        // Bundle identifier jf.SwipeK-Teacher
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationTitle()
    }
    func setupSideMenuAnimation(isAdd:Bool, toView:UIView) {
           if isAdd {
               toView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
               self.navigationController?.view.addSubview(toView)
               toView.frame.origin.y =  view.frame.origin.y
               toView.frame.origin.x = -self.view.frame.width
               toView.alpha = 0
               UIView.animate(withDuration: 1.0) {
                   toView.frame.origin.x = 0
                   toView.alpha = 1
               }
           } else {
               UIView.animate(withDuration: 1.0, animations: {
                   toView.frame.origin.x = -self.view.frame.width
                   toView.alpha = 0
               }, completion: { (status) in
                   toView.removeFromSuperview()
               })
           }
       }
    
    @objc func actionLogout() {
        let alertController = UIAlertController(title: nil, message: "Are you sure do you want to logout?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (actionlogout) in
            UserDefaults.standard.logoutData()
            UserDefaults.standard.logoutIsLogin()
            UserDefaults.standard.logoutTouchId()
            UserDefaults.standard.removeObject(forKey: "location")
            UserDefaults.standard.removeObject(forKey: "LoginSurvey")
            self.navigationController?.popToRootViewController(animated: true)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
           var gradientImage:UIImage?
           UIGraphicsBeginImageContext(gradientLayer.frame.size)
           if let context = UIGraphicsGetCurrentContext() {
               gradientLayer.render(in: context)
               gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
           }
           UIGraphicsEndImageContext()
           return gradientImage
       }
    
    func performAlertViewController(title:String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil)
        )
       
        self.present(alertController, animated: true, completion: nil)
    }
}
