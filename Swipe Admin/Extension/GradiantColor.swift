//
//  GradiantColor.swift
//  IOUHOU
//
//  Created by mac on 23/08/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class GradientView: UIView
{
    override func layoutSubviews()
    {
       // let startColor = AppColor.greenColor
      //  let endColor = AppColor.blueColor
       // applyGradient(colors: [startColor , endColor])
    }
    
    func applyGradient(colors: [UIColor])
    {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.map { $0.cgColor }
        gradient.startPoint = CGPoint(x:0.5,y: 1.0)
        gradient.endPoint = CGPoint(x:0.5, y: 0.0)
        self.layer.insertSublayer(gradient, at: 0)
    }
}

//class GradientViewHorizontal: UIView
//{
//    override func layoutSubviews()
//    {
//        let startColor = AppColor.blueColor
//        let endColor = AppColor.greenColor
//        applyGradient(colors: [startColor , endColor])
//    }
//
//    func applyGradient(colors: [UIColor])
//    {
//        let gradient: CAGradientLayer = CAGradientLayer()
//        gradient.frame = self.bounds
//        gradient.colors = colors.map { $0.cgColor }
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint =  CGPoint(x: 1, y: 0)
//        self.layer.insertSublayer(gradient, at: 0)
//    }
//}

class GradientViewHorizontal: UIView
{
    override func layoutSubviews()
    {
         let startColor = UIColor(named: "firstColor")
         let endColor =  UIColor(named: "thirdColor")
        applyGradient(colors: [startColor! , endColor!])
    }
    
    func applyGradient(colors: [UIColor])
    {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        self.layer.insertSublayer(gradient, at: 0)
        
        layer.cornerRadius = cornerRadius
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 4.0)
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.4
    }
}

class GradientBtn: UIButton
{
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        
        // gradient left: 0078ff (blue) right : 91268f )
        
      //  let startColor = AppColor.blueColor
       // let endColor = AppColor.greenColor
        
        DispatchQueue.main.async
            {
        //        self.applyGradient(colours: [startColor, endColor])
        }
    }
    
    func applyGradient(colours: [UIColor]) -> Void
    {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void
    {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        self.layer.insertSublayer(gradient, below: self.imageView?.layer)
    }
}



class gradiantColor: NSObject {
    
    class func setGradiantInView(gradiantView: UIView) {
        var gradientLayer: CAGradientLayer!
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = gradiantView.bounds
        
        gradientLayer.startPoint = CGPoint(x:1.0,y: 0.5)
        gradientLayer.endPoint = CGPoint(x:0.0, y: 0.5)
        
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)//CGPointMake(0.0, 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.1)//CGPointMake(1.0, 0.5)
        
       // gradientLayer.colors = [AppColor.blueColor.cgColor, AppColor.greenColor.cgColor]
        
        gradiantView.layer.addSublayer(gradientLayer)
    }
    
    class func removeGradiantFromView(gradiantView: UIView ) {
        gradiantView.layer.sublayers = nil
    }
}
