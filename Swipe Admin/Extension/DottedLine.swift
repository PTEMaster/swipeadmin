//
//  DottedLine.swift
//  IOUHOU
//
//  Created by mac on 03/09/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class DottedLine: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    override func layoutSubviews() {
        self.setup()
    }
    
    func setup() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 2
      
        shapeLayer.lineDashPattern = [3,3]
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: 0),
                                CGPoint(x: self.frame.width, y: 0)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}
