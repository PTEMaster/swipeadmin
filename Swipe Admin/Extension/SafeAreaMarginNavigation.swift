//
//  SafeAreaMarginNavigation.swift
//  VPN
//
//  Created by mac on 27/06/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit


enum UIUserInterfaceIdiom : Int {
    case unspecified
    
    case phone // iPhone and iPod touch style UI
    case pad // iPad style UI
}
