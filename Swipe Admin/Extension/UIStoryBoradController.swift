//
//  UIStoryBoradController.swift
//  VPN
//
//  Created by mac on 27/06/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    class func instance(_ storyboard : Storyboard = .authentication) -> UIViewController {
        let storyborad = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        let identifire = NSStringFromClass(self).components(separatedBy: ".").last!
        return storyborad.instantiateViewController(withIdentifier: identifire)
    }
}

enum Storyboard: String {
    
    case authentication = "Authentication"
    case home = "Home"
    case main = "Main"
    case myTrip = "MyTrip"
}
