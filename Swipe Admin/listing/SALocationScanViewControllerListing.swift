//
//  SALocationScanViewController.swift
//  Swipe Admin
//
//  Created by mac on 18/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//  SALocationScanViewControllerListing

import UIKit
import Alamofire


class SALocationScanViewControllerListing: BaseViewController {
    @IBOutlet weak var viewSacn: UIView!
    @IBOutlet weak var viewManual: UIView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnScan: UIButton!
    weak var delegate:LocationDelegate?
    var arrLocation = [GetLocationModal]()
    var arrString = [String]()
    var comeFromScan = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.viewManual.isHidden = true
       // self.viewSacn.isHidden = false
        ScanLocationsApi()
        //let locationString = LocationManager.shard.arrGetlocation.map { $0.RoomName ?? "" }
       // self.arrString = locationString
        self.arrLocation = LocationManager.shard.arrGetlocation
        if let getLocationData =  SelectLocationManager.shard.selectLocation {
           // locationData = getLocationData
            btnLocation.setTitle(getLocationData.RoomName, for: .normal)
        }

        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: SAManualViewController.notificationName, object: nil)
        
        // Do any additional setup after loading the view.
        
      //  self.actionScanMode(self.btnScan)
    }
    
    @objc func onNotification(notification:Notification)
       {
           SelectLocationManager.shard.selectLocation = (notification.userInfo!["data"] as! GetLocationModal)
            btnLocation.setTitle(SelectLocationManager.shard.selectLocation.RoomName, for: .normal)
           //locationData = (notification.userInfo!["data"] as! GetLocationModal)
       }
       
    //MARK:- Button actions
    @IBAction func actionMaual(_ sender: UIButton) {
        self.viewManual.isHidden = false
     //   self.viewSacn.isHidden = true
        NotificationCenter.default.post(name: SAScanViewController.notificationName, object: nil, userInfo:["data":false])
    }
    @IBAction func actionScanMode(_ sender: UIButton) {
         //self.viewManual.isHidden = true
        //self.viewSacn.isHidden = false
        NotificationCenter.default.post(name: SAScanViewController.notificationName, object: nil, userInfo:["data":true])
    }
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionLocation(_ sender: UIButton) {
        let alertController = storyboard?.instantiateViewController(withIdentifier: "SAMenuViewController") as! SAMenuViewController
        alertController.modalPresentationStyle = .popover
        alertController.locationList = LocationManager.shard.arrGetlocation
        alertController.delegate = self
        let height = self.view.frame.size.height / 2
        let width = self.view.frame.size.width / 2
        
        alertController.preferredContentSize = CGSize(width: width, height: height)
        let popover = alertController.popoverPresentationController
        popover?.permittedArrowDirections =  .up
        popover?.delegate = self
        popover?.sourceView = sender
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: - API methods
    func ScanLocationsApi()  {
       
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        Alamofire.request("\(kAPI_BaseURL)\((loginModel?.permissions![0])!)/ScanLocations", headers: headers)
            .responseJSON { response in
                if response.result.isSuccess
                {
                    if let result = response.result.value {
                            let dictResult = result as? [String:Any]
                            let arrLocation = dictResult!["data"] as? [[String:Any]]
                            var locationModel = [GetLocationModal]()
                            for location in arrLocation! {
                                let locations = GetLocationModal(parameters: location)
                                locationModel.append(locations)
                            }
                           LocationManager.shard.arrGetlocation = locationModel
                        if let locations = UserDefaults.standard.dictionary(forKey: "location") {
                            let id = locations["data"] as! Int
                            let items = locationModel.filter({$0.Id == id})
                            // Found crash on 09/04/21/1130 Himanshu pal and add check to fix them.
                           if items.count > 0 {
                                self.btnLocation.setTitle(items[0].RoomName, for: .normal)
                            }
                        // self.btnLocation.setTitle(items[0].RoomName, for: .normal)
                            
                        } else {
                             SelectLocationManager.shard.selectLocation = locationModel[0]
                             self.btnLocation.setTitle(locationModel[0].RoomName, for: .normal)
                        }
                    //    self.dismiss(animated: true, completion: nil)
                     /*   if  self.comeFromScan{
                            self.actionScanMode(self.btnScan)
                        }*/
                        
                       
                    }
                }
        }
        //self.view.endEditing(true)
    }
    
}

//MARK:- UIPopover PresentationController Delegate
extension SALocationScanViewControllerListing:UIPopoverPresentationControllerDelegate , MenuDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
         return .none
     }

        
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
         return true
    }
    
    func selectLocationMenu(items: GetLocationModal) {
        btnLocation.setTitle(items.RoomName, for: .normal)
        SelectLocationManager.shard.selectLocation = items
        NotificationCenter.default.post(name: SAManualViewController.notificationName, object: nil, userInfo:["data":items])
       // let arrlist = items as? NSArray
        
        UserDefaults.standard.set(["data":items.Id], forKey: "location")
    }
 
}


