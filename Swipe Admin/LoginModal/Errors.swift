
import Foundation
struct Errors : Codable {
	let errorCode : String?
	let fieldName : String?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case errorCode = "ErrorCode"
		case fieldName = "FieldName"
		case message = "Message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		errorCode = try values.decodeIfPresent(String.self, forKey: .errorCode)
		fieldName = try values.decodeIfPresent(String.self, forKey: .fieldName)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}
