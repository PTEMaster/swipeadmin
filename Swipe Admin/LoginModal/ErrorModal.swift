

import Foundation
struct ErrorModal: Codable {
    
	let responseStatus : ResponseStatus?
    
    enum CodingKeys: String, CodingKey {
        case responseStatus = "ResponseStatus"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        responseStatus = try values.decodeIfPresent(ResponseStatus.self, forKey: .responseStatus)
    }
    
}
