//
//  GetLocationModal.swift
//  Swipe Admin
//
//  Created by mac on 18/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import Foundation

class GetLocationModal: NSObject {
    var Id:Int?
    var RoomName:String?
    var Station:String?
    var WebScan:String?
    var SchoolId:String?
    var AllowMultipleScans:String?
    var Types:String?
    var Enabled:String?
    
    init(parameters:[String:Any]) {
        Id = parameters["Id"] as? Int
        RoomName = parameters["RoomName"] as? String
        Station = parameters["Station"] as? String
        WebScan = parameters["WebScan"] as? String
        SchoolId = parameters["SchoolId"] as? String
        AllowMultipleScans = parameters["AllowMultipleScans"] as? String
        Types = parameters["Type"] as? String
        Enabled = parameters["Enabled"] as? String
        
        
    }
}

/*{
    "Id": 9281,
    "RoomName": "\"Activity\" (AE)",
    "Station": "Y",
    "WebScan": "Y",
    "SchoolId": "11102495",
    "AllowMultipleScans": false,
    "Type": "Release",
    "Enabled": "Y",
    "Prototype": {}
},*/
