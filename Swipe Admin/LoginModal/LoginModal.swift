
import Foundation
struct LoginModal : Codable {
	let displayName : String?
	let person : Int?
	let allowMarkAllPresent : Bool?
	let permissions : [String]?
	let roles : [String]?
	let userName : String?
	let sessionId : String?
	let responseStatus : ResponseStatus?
    let  LoginSurvey : Int?
    
	enum CodingKeys: String, CodingKey {

		case displayName = "DisplayName"
		case person = "Person"
		case allowMarkAllPresent = "AllowMarkAllPresent"
		case permissions = "Permissions"
		case roles = "Roles"
		case userName = "UserName"
		case sessionId = "SessionId"
		case responseStatus = "ResponseStatus"
        case LoginSurvey = "LoginSurvey"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		displayName = try values.decodeIfPresent(String.self, forKey: .displayName)
		person = try values.decodeIfPresent(Int.self, forKey: .person)
		allowMarkAllPresent = try values.decodeIfPresent(Bool.self, forKey: .allowMarkAllPresent)
		permissions = try values.decodeIfPresent([String].self, forKey: .permissions)
		roles = try values.decodeIfPresent([String].self, forKey: .roles)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		sessionId = try values.decodeIfPresent(String.self, forKey: .sessionId)
		responseStatus = try values.decodeIfPresent(ResponseStatus.self, forKey: .responseStatus)
        LoginSurvey = try values.decodeIfPresent(Int.self, forKey: .LoginSurvey)
	}

}
