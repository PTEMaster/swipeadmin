//
//  StudentModal.swift
//  Swipe Admin
//
//  Created by mac on 18/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import Foundation

class StudentModal: NSObject {
    
    var Active:Int?
    var FirstName:String?
    var Grade:String?
    var Homeroom:String?
    var LastName:String?
    var PersonId:Int?
    var StudentId:Int?
    var StudentNumber:String?
    var UniqueIdentifier:String?
    var imageHost:String?
    var imageUrl: String?
    var id : String?
    var SchoolId:String?
    var Bus:String?
    
    init(parameters:[String:Any]) {
        Active = parameters["Active"] as? Int
        FirstName = parameters["FirstName"] as? String
        Grade = parameters["Grade"] as? String
        Homeroom = parameters["Homeroom"] as? String
        LastName = parameters["LastName"] as? String
        PersonId = parameters["PersonId"] as? Int
        StudentId = parameters["StudentId"] as? Int
        StudentNumber = parameters["StudentNumber"] as? String
        UniqueIdentifier = parameters["UniqueIdentifier"] as? String
        imageHost = parameters["imageHost"] as? String
        imageUrl = parameters["ImageUrl"] as? String
        id = parameters["Id"] as? String
        SchoolId = parameters["SchoolId"] as? String
        Bus = parameters["Bus"] as? String
    }
}
/*
13 elements
▿ 0 : 2 elements
  - key : "Homeroom"
  - value : 1
▿ 1 : 2 elements
  - key : "FirstName"
  - value : Student
▿ 2 : 2 elements
  - key : "LunchOptions"
  ▿ value : 3 elements
    - 0 : F
    - 1 : R
    - 2 :
▿ 3 : 2 elements
  - key : "Active"
  - value : Y
▿ 4 : 2 elements
  - key : "Bus"
  - value : N/A
▿ 5 : 2 elements
  - key : "UniqueIdentifier"
  - value : d6825dec-37f7-4f2b-b3db-ddeb811011be
▿ 6 : 2 elements
  - key : "SchoolId"
  - value : 11102495
▿ 7 : 2 elements
  - key : "Id"
  - value : 508874
▿ 8 : 2 elements
  - key : "PersonId"
  - value : 0
▿ 9 : 2 elements
  - key : "StudentNumber"
  - value : 123456789
▿ 10 : 2 elements
  - key : "Grade"
  - value : 06
▿ 11 : 2 elements
  - key : "ImageUrl"
  - value : /SWIPEDATA/11102495/Pictures/123456789.jpg
▿ 12 : 2 elements
  - key : "LastName"
  - value : 1*/
