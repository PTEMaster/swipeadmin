//
//  ScansModal.swift
//  Swipe Admin
//
//  Created by mac on 31/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
//

import Foundation

class ScansModal: NSObject {
    var type:String?
    var text:String?
    var source:String?
    
    init(parameters:[String:Any]) {
        type = parameters["__type"] as? String
        text = parameters["Text"] as? String
        source = parameters["Source"] as? String
    }
    
}
