//
//  ViewController.swift
//  Swipe Admin
//
//  Created by CTInformatics on 12/01/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import SwiftyJSON
import LocalAuthentication
import IQKeyboardManagerSwift

class LoginViewController: BaseViewController, UITextFieldDelegate {
    // MARK: - Properties
    
    @IBOutlet weak var btnlogin:UIButton!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var btnAuthnticateImage: UIButton!
    @IBOutlet weak var btnTitle: UIButton!
    let context : LAContext = LAContext()
    var authError: NSError?
  
   let def = UserDefaults.standard
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.setupLoginAndAuth()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
   
     func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
          //  test.backgroundColor = UIColor.purple
             print("Landscapebbbbb")
        } else {
          //  test.backgroundColor = UIColor.blue
             print("Portraitbbbbb")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    func setupLoginAndAuth() {
        if !UserDefaults.standard.getUserName().isEmpty {
            txtEmail.text = UserDefaults.standard.getUserName()
        }
        
        self.setupLogupCondition()
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: &authError) {
            if #available(iOS 11.0, *) {
                switch context.biometryType {
                case .faceID:
                    btnTitle.setTitle("Authenticate using your face", for: .normal)
                    btnAuthnticateImage.setImage(UIImage(named: "faceId"), for: .normal)
                case .touchID:
                    btnTitle.setTitle("Authenticate using your Finger", for: .normal)
                    btnAuthnticateImage.setImage(UIImage(named: "TouchId"), for: .normal)
                case .none:
                    btnTitle.isHidden = true
                    btnAuthnticateImage.isHidden = true
                default:
                    break
                }
            }
        }
    }
    
    func setupLogupCondition() {
           if UserDefaults.standard.isLoggedIn() == true, UserDefaults.standard.isTouchId() == true {
               self.authenticateUserTouchID()
           }
       }
    
    func CanLogin() -> Bool {
        if (txtEmail.text?.isEmpty)! {
            Util.showAlertWithMessage("Please enter username", title: kAPI_Message)
            return false
        }
        if (txtPassword.text?.isEmpty)! {
            Util.showAlertWithMessage("Please enter password", title: kAPI_Message)
             return false
        }
         return true
    }
    
    
    // MARK: - Action methods
  
    @IBAction func Click_Login(sender:Any){
        if CanLogin() {
            LoginAPI()
        }
        self.view.endEditing(true)
    }
    
    @IBAction func actionAuthenticateLogin(_ sender: Any) {
          self.authenticateUserTouchID()
       }
   
    
    // MARK: - API methods
    
    func LoginAPI()
    {
        if (!isNetworkAvailable) {
            Util.showNetWorkAlert()
            return
        }
        
        let postParams: [String: AnyObject] =
            [
            "UserName"          : Util.getValidString(txtEmail.text) as AnyObject,
            "Password"          : Util.getValidString(txtPassword.text) as AnyObject
        ]
        
        appDelegate.StrUsername = Util.getValidString(txtEmail.text)
        Networking.performApiCall(Networking.Router.userLogin((postParams as [String : AnyObject]?)!), callerObj: self, showHud:true) { (response) -> () in
            appDelegate.hideHUD(self.view)
            if response.result.isSuccess {
                if let result = response.result.value {
                    self.txtPassword.text = ""
                    if  let Dataresponse = result as? NSDictionary{
                        print(Dataresponse)
                        let LoginSurvey = Dataresponse["LoginSurvey"] as? Int
                        let UserName = Dataresponse["UserName"] as? String
                        let person = Dataresponse["IdNumber"] as? Int
                        let displayName = Dataresponse["DisplayName"] as? String
                        UserDefaults.standard.set("\(LoginSurvey ?? 0)", forKey: "LoginSurvey")
                        UserDefaults.standard.set(UserName, forKey: "UserName")
                        UserDefaults.standard.set("\(String(describing: person))", forKey: "person")
                        UserDefaults.standard.set(displayName, forKey: "displayName")
                    }
                   
                     
                 //   UserDefaults.standard.set("\(loginModel?.LoginSurvey ?? 1)", forKey: "LoginSurvey")
                    UserDefaults.standard.setUserName(value: self.txtEmail.text!)
                    UserDefaults.standard.setLoggedIn(value: true)
                    UserDefaults.standard.setTouchid(value: true)
                    UserDefaults.standard.setUserID(value: response.data!)
                    DispatchQueue.main.async {
                     self.performSegue(withIdentifier: "gotoStudentList", sender: self)
                    }
                    
            } else {
                   Util.showAlertWithMessage("Password Does not match", title: "Error")
                }
            } else {
                   Util.showAlertWithMessage("Password Does not match", title: "Error")
            }
        }
        
    }
    
    func authenticateUserTouchID() {
        
        // Declare a NSError variable.
        let myLocalizedReasonString = "Authentication is needed to access your Home."
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            context.evaluatePolicy( .deviceOwnerAuthenticationWithBiometrics , localizedReason: myLocalizedReasonString) { success, evaluateError in
                // IF TOUCH ID AUTHENTICATION IS SUCCESSFUL, NAVIGATE TO NEXT VIEW CONTROLLER
                if success {
                    DispatchQueue.main.async{
                       // print("Authentication success by the system")
                        if UserDefaults.standard.isLoggedIn() {
                            self.performSegue(withIdentifier: "gotoStudentList", sender: self)
                            UserDefaults.standard.setTouchid(value: true)
                        } else {
                            self.performAlertViewController(title: AppName, message: ConstantsMessage.valid.empty.kmanually)
                        }
                        
                    }
                }
                else // IF TOUCH ID AUTHENTICATION IS FAILED, PRINT ERROR MSG
                {
                    guard let error = self.authError else {
                        return
                    }
                    
                    let message = self.showErrorMessageForLAErrorCode(errorCode: error.code)
                    print(message)
                }
            }
        }
    }
    
     
      func showErrorMessageForLAErrorCode( errorCode:Int ) -> String {
          
          var message = ""
          
          switch errorCode {
              
          case LAError.appCancel.rawValue:
              message = "Authentication was cancelled by application"
              
          case LAError.authenticationFailed.rawValue:
              message = "The user failed to provide valid credentials"
              
          case LAError.invalidContext.rawValue:
              message = "The context is invalid"
              
          case LAError.passcodeNotSet.rawValue:
              message = "Passcode is not set on the device"
              
          case LAError.systemCancel.rawValue:
              message = "Authentication was cancelled by the system"
    
          case LAError.userCancel.rawValue:
              message = "The user did cancel"
              
          case LAError.userFallback.rawValue:
              message = "The user chose to use the fallback"
              
          default:
              message = "Did not find error code on LAError object"
              
          }
          
          return message
          
      }
    func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)

        layer.render(in: UIGraphicsGetCurrentContext()!)

        let outputImage = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return outputImage!
    }
    
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       return textField.resignFirstResponder()
   }
}


extension UIViewController {
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
}
extension UIView {
    
    func add(color: UIColor, width: CGFloat) {
        let borderLayer = CALayer()
        borderLayer.backgroundColor = color.cgColor
     //   borderLayer.name = border.rawValue
            borderLayer.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(borderLayer)
    }
}


enum BiometricType {
    case none
    case touch
    case face
}
