//
//  ScanViewController.swift
//  Swipe Admin
//
//  Created by mac on 18/03/20.
//  Copyright © 2020 SwipeK12. All rights reserved.
// SAScanViewControllerScan

import UIKit
import AVFoundation
import Alamofire
import MBProgressHUD

class SAScanViewControllerScan: BaseViewController {
    @IBOutlet weak var ViewCamera: UIView!
    @IBOutlet weak var lblstudentName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet var viewPop: UIView!
    @IBOutlet weak var btnLocation: UIButton!
    var arrLocation = [GetLocationModal]()
    var player: AVAudioPlayer??
    var captureSession = AVCaptureSession()
    var histroy = String()
    var scheduleId = Int()
    var timer  = Timer()
    var statusCheck: Bool = true
    var locationData: GetLocationModal?
    var apiactionScanMode = String()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var arrString = [String]()
    var startTime:Date!
    
    var  student : StudentModal?
    static let notificationName = Notification.Name("scanViewController")
    // var qrCodeFrameSecond: UIView?
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr]
    var scanlocation = SALocationScanViewController()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        //let locationString = LocationManager.shard.arrGetlocation.map { $0.RoomName ?? "" }
       // self.arrString = locationString
        self.arrLocation = LocationManager.shard.arrGetlocation
        if let getLocationData =  SelectLocationManager.shard.selectLocation {
            locationData = getLocationData
            btnLocation.setTitle(getLocationData.RoomName, for: .normal)
        }
        
      if let locations = UserDefaults.standard.dictionary(forKey: "location") {
            let id = locations["data"] as! Int
            let items = arrLocation.filter({$0.Id == id})
        if items.count > 0 {
            locationData = items[0]
            self.btnLocation.setTitle(items[0].RoomName, for: .normal)
        }
            
        }
        
        //lblHistory.text = histroy
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer?.videoGravity = .resizeAspectFill
        //videoPreviewLayer?.connection?.videoOrientation = .portrait
        self.ViewCamera.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        if let qrCodeFrame = qrCodeFrameView {
            qrCodeFrame.layer.borderColor = UIColor.green.cgColor
            qrCodeFrame.layer.borderWidth = 2
            self.ViewCamera.addSubview(qrCodeFrame)
            self.ViewCamera.bringSubviewToFront(qrCodeFrame)
        }
       
    }
        // MARK: - Helper methods
    func launchApp(decodedURL: String) {
        
        if presentedViewController != nil {
            return
        }
        
        let alertPrompt = UIAlertController(title: "Open App", message: "You're going to open \(decodedURL)", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            
            if let url = URL(string: decodedURL) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        alertPrompt.addAction(confirmAction)
        alertPrompt.addAction(cancelAction)
        
        present(alertPrompt, animated: true, completion: nil)
    }
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        //self.ViewCamera.layer.addSublayer(videoPreviewLayer!)
        print(self.ViewCamera.layer.bounds)
       // ViewCamera.frame.size.height = ViewCamera.frame.size.height - 12
        videoPreviewLayer?.frame = self.ViewCamera.layer.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        videoPreviewLayer?.frame = self.ViewCamera.bounds
        if let connection =  self.videoPreviewLayer?.connection  {
            let currentDevice: UIDevice = UIDevice.current
            let orientation: UIDeviceOrientation = currentDevice.orientation
            let previewLayerConnection : AVCaptureConnection = connection
            
            if previewLayerConnection.isVideoOrientationSupported {
                print(orientation)
                switch (orientation) {
                case .portrait:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                case .landscapeRight:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                    break
                case .landscapeLeft:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                    break
                case .portraitUpsideDown:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                    break
                default:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                }
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: SAManualViewController.notificationName, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(isScanNotification(notification:)), name: SAScanViewControllerScan.notificationName, object: nil)

    }
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Switch camera
    @IBAction func actionSwitchCamera(_ sender: UIButton) {
        let currentCameraInput: AVCaptureInput = captureSession.inputs[0]
        captureSession.removeInput(currentCameraInput)
        var newCamera: AVCaptureDevice
        newCamera = AVCaptureDevice.default(for: AVMediaType.video)!
        
        if (currentCameraInput as! AVCaptureDeviceInput).device.position == .back {
            UIView.transition(with: self.ViewCamera, duration: 0.3, options: .transitionCrossDissolve, animations: {
                newCamera = self.cameraWithPosition(.front)!
                sender.isSelected = true
            }, completion: nil)
        } else {
            UIView.transition(with: self.ViewCamera, duration: 0.3, options: .transitionCrossDissolve, animations: {
                newCamera = self.cameraWithPosition(.back)!
                sender.isSelected = false
            }, completion: nil)
        }
        do {
            try self.captureSession.addInput(AVCaptureDeviceInput(device: newCamera))
        }
        catch {
            print("error: \(error.localizedDescription)")
        }
    }
    
    func cameraWithPosition(_ position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let deviceDescoverySession = AVCaptureDevice.DiscoverySession.init(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        
        for device in deviceDescoverySession.devices {
            if device.position == position {
                return device
            }
        }
        return nil
    }
    

    @IBAction func actionSelectedLocation(_ sender: UIButton) {
        let alertController = storyboard?.instantiateViewController(withIdentifier: "SAMenuViewController") as! SAMenuViewController
        alertController.modalPresentationStyle = .popover
        alertController.locationList = LocationManager.shard.arrGetlocation
        alertController.delegate = self
        let height = self.view.frame.size.height / 2
        let width = self.view.frame.size.width / 2
        
        alertController.preferredContentSize = CGSize(width: width, height: height)
        let popover = alertController.popoverPresentationController
        popover?.permittedArrowDirections =  .up
        popover?.delegate = self
        popover?.sourceView = sender
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func isScanNotification(notification:Notification) {
        let isScan = notification.userInfo!["data"] as! Bool
        if isScan {
            videoPreviewLayer?.session?.startRunning()
        } else {
            videoPreviewLayer?.session?.stopRunning()
        }
    }
    @objc func onNotification(notification:Notification) {
        print(notification.userInfo as Any)
        locationData = (notification.userInfo!["data"] as! GetLocationModal)
    }
    
        override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
            videoPreviewLayer?.frame = self.ViewCamera.bounds
           if let connection =  self.videoPreviewLayer?.connection  {
             let currentDevice: UIDevice = UIDevice.current
             let orientation: UIDeviceOrientation = currentDevice.orientation
             let previewLayerConnection : AVCaptureConnection = connection
             
             if previewLayerConnection.isVideoOrientationSupported {
                print(orientation)
               switch (orientation) {
               case .portrait:
                 updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                 break
               case .landscapeRight:
                 updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                 break
               case .landscapeLeft:
                 updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                 break
               case .portraitUpsideDown:
                 updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                 break
               default:
                 updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                 break
               }
             }
           }
         }
        
        @IBAction func actionDone(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
    func setupSoundValidandInvalid(sound:String) {
        let url = Bundle.main.url(forResource: sound, withExtension: "mp3")!
        do {
            //Preparation to play
            do {
              
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
             do {
             try AVAudioSession.sharedInstance().setActive(true)
             //print("AVAudioSession is Active")
             } catch _ as NSError {
             //print(error.localizedDescription)
             }
             } catch let error {
             print("Error in AVAudio Session\(error.localizedDescription)")
             }
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player?.prepareToPlay()
            player?.play()
            
        } catch let error as NSError {
            print(error.description)
        }
    }
    
    func setupPopView(status:Bool, toView:UIView) {
        toView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 60)
        toView.frame.origin.y = self.view.frame.height
        self.view.addSubview(toView)
        UIView.animate(withDuration: 0.5) {
            toView.frame.origin.y = self.view.frame.height - (toView.frame.height)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            UIView.animate(withDuration: 0.5, delay: 0.2, options: [.curveEaseInOut], animations: {
                // self.statusCheck = true
            }) { (bool) in
                toView.removeFromSuperview()
            }
        }
    }
    
}

extension SAScanViewControllerScan : AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            //qrCodeFrameView?.frame = CGRect.zero
            //  messageLabel.text = "No QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
             captureSession.stopRunning()
            //let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
          //  qrCodeFrameView?.frame = barCodeObject!.bounds
            if metadataObj.stringValue != nil {
                // timer.invalidate()
                //  timer = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(actionInfoApicall), userInfo: metadataObj.stringValue, repeats: false)
                startTime = Date()
                if statusCheck {
                   // MBProgressHUD.showAdded(to: self.view, animated: true)
                    statusCheck = false
                    let executionTime = Date().timeIntervalSince(self.startTime)
                    print(executionTime)
                    startTime = Date()
                    DispatchQueue.main.async {
                        let login = UserDefaults.standard.getUserID()
                        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
                        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                        let permissions = (loginModel?.permissions![0])!
                        let getUrl = kAPI_BaseURL +  "\(permissions)" + "/Student?studentNumber" + "=\( metadataObj.stringValue ?? "")"
                        print(getUrl)
                        guard let url = URL(string: getUrl) else {
                            self.isSetupInvalid(title: "Invalid scan", Date: "")
                            return
                        }
                        self.isStudentNumberApiCall(url: url)
                    }
                }
                print(metadataObj.stringValue as Any)
            }
        }
        
    }
        
    func isStudentNumberApiCall(url:URL) {
        let headers: HTTPHeaders = ["Accept": "application/json" ]
        Alamofire.request(url , headers: headers)
            .responseJSON { response in
                if response.result.isSuccess
                {
                    if let result = response.result.value {
                    
                        let studentData = result as? [String:Any]
                      print(studentData)
                       if let stud = studentData!["data"]  {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        self.student = StudentModal(parameters: stud as! [String : Any])
                        print(self.student?.PersonId!)
                        select2(PersonId: String(self.student!.PersonId!))
                     
                        } else {
                            self.isSetupInvalid(title: "Invalid scan", Date: "")
                        }
                    } else {
                        self.isSetupInvalid(title: "Invalid scan", Date: "")
                    }
                } else if response.result.isFailure {
                    self.isSetupInvalid(title: "Invalid scan", Date: "")
                }
        }
        
        
        func select2(PersonId: String){
            appDelegate.showHUD("", onView: self.view)
            var params = [String: Any]()
     
            params = ["PersonId":PersonId]
            
            print(params)
            let url = "\(kAPI_BaseURL)CheckSurveySubmission/"
           POSTrowValue(url: url, param: params, header: nil, isLoader: false)
        }
        
        
        func POSTrowValue(url: String, param: Parameters, header: HTTPHeaders?,isLoader: Bool){
           Alamofire.request(url, method:.post, parameters: param,encoding: JSONEncoding.default, headers: header) .responseJSON { response in
            appDelegate.hideHUD(self.view)
            if response.result.isSuccess{
                if let result = response.result.value {
                    print(result)
                    if let resultarr = result as? NSArray  {
                        let date = Date()
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let currentTime = dateFormatter.string(from: date)
                        if resultarr.count == 0 {
                            let strfname = self.student!.FirstName!
                            let strlname = self.student!.LastName!
                            
                            self.isSetupInvalid(title: strfname + strlname , Date: currentTime)
                           
                        } else {
                            
                            let dict = resultarr.lastObject as? NSDictionary
                            let surveyResult =  dict?["surveyResult"] as? String ?? ""
                            if surveyResult  == "Fail"{
                                let strfname = self.student!.FirstName!
                                let strlname = self.student!.LastName!
                                self.isSetupInvalid(title: strfname + strlname, Date: currentTime)
                            }else{
                                self.isSubmitWebScanApiCall(studentId: self.student!.StudentNumber!)
                            }
                            
                            // call api
                            
                        }
                    }
                }
            }else{
                print(response.result.error?.localizedDescription)
            }
           }
       }
    }
    
    func isSubmitWebScanApiCall(studentId:String) {
        let url = "\(kAPI_BaseURL)Attendance/SubmitWebScan"
        let login = UserDefaults.standard.getUserID()
        let loginModel = try? JSONDecoder().decode(LoginModal.self, from: login)
        let permissions = (loginModel?.permissions![0])!
        var params = [String:Any]()
        params["delimitedStudentString"] =  studentId
        params["schoolId"] = permissions
        params["scanLocation"] = "Covid Scanning"
        params["classLocationScanned"] = false
        params["commandedBy"] = UserDefaults.standard.getUserName()
        let headers: HTTPHeaders = ["Accept": "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        //    print(response)
            if response.result.isSuccess {
                if let result = response.result.value {
                    print(result)
                     self.lblstudentName.text = self.student!.FirstName! + " " +  self.student!.LastName!
                          let date = Date()
                          let dateFormatter = DateFormatter()
                          dateFormatter.dateFormat = "HH:mm"
                          let currentTime = dateFormatter.string(from: date)
                          self.lblTime.text = "\(currentTime)"
                          self.setupSoundValidandInvalid(sound: "validId")
                          self.viewPop.backgroundColor = .green
                          //self.setupPopView(status: true, toView: self.viewPop)
                          
                          let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                      vc.strStudentId = self.student?.StudentNumber ?? ""
                      vc.strStudentName = self.student?.FirstName ?? "" + self.student!.LastName!
                          vc.modalPresentationStyle = .overFullScreen
                          vc.modalTransitionStyle = .crossDissolve
                          self.present(vc, animated: true, completion: nil)
                          DispatchQueue.main.asyncAfter(deadline: .now() + 2.2) {
                              self.captureSession.startRunning()
                              self.statusCheck = true
                          }
                     
                } else {
                    self.isSetupInvalid(title: "Invalid scan", Date: "")
                }
            } else {
                self.isSetupInvalid(title: "Invalid scan", Date: "")
            }
            
        }
    }
    
    func isSetupInvalid(title : String , Date : String) {
      //  MBProgressHUD.hide(for: self.view, animated: true)
        self.setupSoundValidandInvalid(sound: "invalidId")
        self.lblstudentName.text = title
        self.lblTime.text = Date
        self.viewPop.backgroundColor = .systemRed
        self.setupPopView(status: true, toView: self.viewPop)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.captureSession.startRunning()
            self.statusCheck = true
        }
    }
    
    func isValidUrl(url: String) -> Bool {
        let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: url)
        return result
    }
}

//MARK:- UIPopover PresentationController Delegate
extension SAScanViewControllerScan:UIPopoverPresentationControllerDelegate , MenuDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
         return .none
     }

        
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
         return true
    }
    
    func selectLocationMenu(items: GetLocationModal) {
        btnLocation.setTitle(items.RoomName, for: .normal)
        SelectLocationManager.shard.selectLocation = items
        locationData = items
        UserDefaults.standard.set(["data":items.Id], forKey: "location")
        NotificationCenter.default.post(name: SAManualViewController.notificationName, object: nil, userInfo:["data":items])
    }
 
}
