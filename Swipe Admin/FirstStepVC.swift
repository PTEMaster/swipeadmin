//
//  FirstStepVC.swift
//  SwipeK Teacher
//
//  Created by mac on 22/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class FirstStepVC: BaseViewController, ApiManagerDelegate {

    
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    var count = 0
    var arrQuestion = [[String: Any]]()
    var isFail : Bool = false
    var mystring = ""
    var studentDetail = [String: Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
appDelegate.isLoginTrue = false
        // Do any additional setup after loading the view.
        SurveyQuestionAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    

    @IBAction func actionYes(_ sender: Any) {
        appDelegate.isLoginTrue = true
        questionArr("Y")
    }
    @IBAction func actionNo(_ sender: Any) {
        questionArr("N")
    }
    
    
    func SurveyQuestionAPI() {
        let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey") ?? "1"
        APIManager.share.delegate = self
        appDelegate.showHUD("", onView: self.view)
        let urlString = "\(kAPI_BaseURL)Survey" + "/" + LoginSurvey
        print(urlString)
        mystring =  urlString
        APIManager.share.performGetAPI(dict: [:], APIAction: urlString)
        
    }
    
    func SurveyResultAPI() {
        
       
        appDelegate.showHUD("", onView: self.view)

        let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey")
        let id = UserDefaults.standard.string(forKey: "Permissions")
        let userName = UserDefaults.standard.string(forKey: "UserName")
        let StudentNumber2 = studentDetail["StudentId"] as? Int
        
        var postParams: [String: AnyObject] =
            [
                "SurveyId"          : "\(LoginSurvey!)" as AnyObject,
                "StudentId"      : StudentNumber2 as AnyObject,
                "SubmitBy"         : userName as AnyObject
        ]
        if isFail{
            postParams["SurveyResult"] = "Fail" as AnyObject
        }else{
            postParams["SurveyResult"] = "Pass" as AnyObject
        }
        print(postParams)
        let urlString = "\(kAPI_BaseURL)SurveyResult"
        mystring =  urlString
        
        APIManager.share.delegate = self
        self.view.endEditing(true)
        
        APIManager.share.performPostAPI(dict: postParams, APIAction: urlString)

    }
    
    func questionArr(_ userAnswer : String){
       if arrQuestion.count > 0{
            let obj =   arrQuestion.first
            let answer = obj?["ExpectedAnswer"]as? String
            if userAnswer != answer{
                isFail  = true
                SurveyResultAPI()
                return
            }
            arrQuestion.removeFirst()
            if arrQuestion.count > 0{
          let obj =   arrQuestion.first
            lblQuestion.text = obj?["Question"]as? String
            }else{
                print("No data2")
                SurveyResultAPI()
            }
        }else{
            print("No data")
            SurveyResultAPI()
        }
        print(isFail)
    }
    
    func successResponse(response: Any, check: String) {
        appDelegate.hideHUD(self.view)
       // let LoginSurvey = UserDefaults.standard.string(forKey: "LoginSurvey") ?? "1"
        
         if check == "\(kAPI_BaseURL)SurveyResult" {
            if self.isFail{
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "FifthStepVC") as! FifthStepVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "SecondStepVC") as! SecondStepVC
                vc.studentDetail = studentDetail
                    self.navigationController?.pushViewController(vc, animated: true)
            }
         }else{
            guard let data = response as? Data else {
                return
            }
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                //let code  = resultObject["code"] as? String
                let Questions = resultObject["Questions"] as? [[String : Any]]
                if Questions?.count ?? 0 > 0{
                    self.arrQuestion = Questions!
                    let obj =   self.arrQuestion.first
                    self.lblQuestion.text = obj?["Question"]as? String
                }
            } catch {
                print("Unable to parse JSON response")
            }
         }
    }
    
    func errorResponse(error: Any, check: String) {
        appDelegate.hideHUD(self.view)
        if check == mystring {
            if let error = error as? NSError {
                self.performAlertViewController(title: AppName, message:error.localizedDescription)
                return
            }
            
            guard let data = error as? Data else {
                return
            }
            guard let responseError = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
            let errorMessage = responseError.responseStatus?.message
            self.performAlertViewController(title: AppName, message: errorMessage ?? "IndexOutOfRangeException")
        }else if check == mystring {
            if let error = error as? NSError {
                self.performAlertViewController(title: AppName, message:error.localizedDescription)
                return
            }
            
            guard let data = error as? Data else {
                return
            }
            guard let responseError = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
            let errorMessage = responseError.responseStatus?.message
            self.performAlertViewController(title: AppName, message: errorMessage ?? "IndexOutOfRangeException")
        }
    }
    
}
