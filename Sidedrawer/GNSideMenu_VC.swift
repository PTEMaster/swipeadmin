//
//  GNSideMenu_VC.swift
//  GermanNew
//
//  Created by mac on 07/02/18.
//  Copyright © 2018 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD
class GNSideMenu_VC: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var Menulist = ["Home", "Basic Courses", "Creators", "Learn German via Skype", "Feedback"]
    var Menuimage = ["home", "course", "creator", "skypemenu", "feedback"]
    
    var FollowUslist = ["Youtube", "Facebook", "Instagram"]
    var FollowUsimage = ["youtube", "facebook", "instagram"]
    
    
     let picker = UIImagePickerController()
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imageName:UIButton!
     @IBOutlet weak var viewBlur:UIView!

   
    override func viewDidLoad() {
        super.viewDidLoad()

        viewBlur.frame = self.view.bounds
        picker.delegate = self
        
        self.view.addSubview(viewBlur)
         viewBlur.isHidden = true
      if  let userdetail = K_default.object(forKey:K_Userdetail)
      {
        let dict = userdetail as! NSDictionary
        imageName.layer.cornerRadius = imageName.frame.size.width/2
        imageName.layer.borderColor = UIColor.lightGray.cgColor
        imageName.layer.borderWidth = 1
        if  let imagename = dict[K_userPic]
        {
            Networking.downloadImage(fromUrl: Util.getValidString("\(kAPI_BaseURL)\(kAPI_ProfilePic_path)\(imagename)"), withPlaceHolder: "", completionHandler: { (image) -> Void in
                self.imageName.imageView?.image  = image
            })
        }
        }
   
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//MARK: - Action methods
    @IBAction func SelectPhoto(sender:Any)
    {
        
        if viewBlur.isHidden {
            viewBlur.isHidden = false
        }
    }
    @IBAction func SelectCamera(sender:Any)
    {
      self.shootPhoto()
    }
    @IBAction func SelectGallary(sender:Any)
    {
        
       self.photoFromLibrary()
    }
    @IBAction func SelectCross(sender:Any)
    {
        
     viewBlur.isHidden=true
    }
    
    //MARK: - API methods
    
    func uploadapi() {
        //        if (!isNetworkAvailable) {
        //            Util.showNetWorkAlert()
        //            return
        //        }
       
        let postParams: [String: AnyObject] =
            [
                "user_id"          : KUserInfo[K_userID] as AnyObject,
                kAPI_AuthName   : kAPI_AuthKey as AnyObject
                ]
        
        let hud             = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode            = MBProgressHUDMode.determinate
        hud.bezelView.color = UIColor.lightGray
        hud.label.font      = UIFont(name: AppFontLight, size: 17)
        hud.label.text      = "Uploading..."
        print("Post Parameter is:\(postParams)")
        Networking.uploadImagesWithParams(Networking.Router.ChangeProfilePic(postParams as [String : AnyObject]), imageArray: [(imageName.imageView?.image!)!], strImageKey: "profile_pic", dictParams: postParams, callerObj: self, showHud: true) { (encodingResult) -> Void in
            switch encodingResult {
            case .success(let upload, _, _):
                print(upload)
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    hud.progress = Float(Progress.fractionCompleted)
                })
                upload.responseJSON { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    print(response)
                    if response.result.isSuccess {
                        if let result = response.result.value {
                            let data  = result as! NSDictionary
                            
                            if data["status"] as! NSNumber == 0
                            {
                                Util.showAlertWithMessage(data["responce"] as! String, title: "Alert")
                            }else{
                               // let dict = data["responce"] as! NSDictionary
                                let UserResponse = data["responce"] as! NSDictionary
                                K_default.setValue(UserResponse, forKey:K_Userdetail)
                            }
                            
                        }
                    }
                    
                }
            case .failure(let encodingError):
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                let alertController = UIAlertController(title: encodingError.localizedDescription, message: "Error", preferredStyle: UIAlertControllerStyle.alert
                )
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,
                                                        handler: nil)
                )
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    //MARK: - Private methods
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    func photoFromLibrary() {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    func shootPhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            noCamera()
        }
    }
    //MARK: - Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        imageName.imageView?.image = chosenImage
        self.uploadapi()
      //  self.imageData = UIImageJPEGRepresentation(chosenImage, 0.4)
      //  imgPhoto.image = chosenImage
        
        dismiss(animated:true, completion: nil) //5
       // self.UploadPhoto()
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}



//MARK: - Tableview delegates
extension GNSideMenu_VC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0
        {
            return ""
        }else{
            
            return "Follow Us"
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return Menulist.count
        }else{
            return FollowUslist.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewVibrantCell") as! UITableViewVibrantCell
        
        if indexPath.section == 0 {
         cell.lbltitle.text = Menulist[indexPath.row] as String
            cell.img.image = UIImage.init(named: Menuimage[indexPath.row] as String)
        }else{
              cell.lbltitle.text = FollowUslist[indexPath.row] as String
             cell.img.image = UIImage.init(named: FollowUsimage[indexPath.row] as String)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0
            {
            let vc = storyboard?.instantiateViewController(withIdentifier: "GNCreator_VC") as! GNCreator_VC
            self.navigationController?.pushViewController(vc, animated: true)
            }
            if indexPath.row == 1
            {
                
            }
            
            if indexPath.row == 2
            {
                
            }
            if indexPath.row == 3
            {
                
            }
            if indexPath.row == 4
            {
                
            }
        }else
        {
            if indexPath.row == 0
            {
                guard let url = URL(string: "http://www.google.com") else {
                    return //be safe
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
       
        
    }
}
